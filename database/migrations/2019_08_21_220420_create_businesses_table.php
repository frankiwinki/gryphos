<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('business_name')->nullable();
            $table->string('business_ruc')->nullable();
            $table->string('database_driver')->nullable();
            $table->string('ip')->nullable();
            $table->string('connection_string')->nullable();
            $table->binary('status')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('businesses');
    }
}
