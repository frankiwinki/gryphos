<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReporteVentaDetalle extends Model
{
    protected $connection = 'tenant';
    protected $table = 'v_ventadetalles';
}
