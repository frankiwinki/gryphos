<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPago extends Model
{
    protected $connection = 'tenant';
    protected $table = 'tipo_pago';
}
