<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $connection = 'tenant';
    protected $table = 'clientes';

    protected $fillable = [
        'nro_doc','nombres', 'distrito', 'provincia', 'departamento'
    ];
}
