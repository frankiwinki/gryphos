<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditoPagoFactura extends Model
{
    protected $connection = 'tenant';
    protected $table = 'creditos_pagos_facturas';

    protected $fillable = ['id_pago','num_doc','monto'];
}
