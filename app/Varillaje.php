<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Varillaje extends Model
{
    protected $connection = 'tenant';
    //
    protected $table = 'varillajes';
}
