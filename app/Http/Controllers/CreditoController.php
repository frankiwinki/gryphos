<?php

namespace App\Http\Controllers;

use App\Credit;
use App\CreditoProducto;
use App\Cliente;
use App\Producto;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;
use App\Exports\CreditosExport;
use Maatwebsite\Excel\Facades\Excel;

class CreditoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            PosController::ChangeDB();
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;
        if (!empty($keyword)) {
            $creditos = DB::connection('tenant')->table('creditos')
                ->join('clientes', 'creditos.id_cliente', '=', 'clientes.id')
                ->select('creditos.*', 'clientes.nro_doc', 'clientes.nombres')
                ->where('creditos.estado',"=","A")
                ->where('clientes.nombres', 'LIKE', "%$keyword%")->orWhere('clientes.nro_doc', 'LIKE', "%$keyword%")
                ->orderBy('id', 'desc')
                ->paginate($perPage);
        } else {
            $creditos = DB::connection('tenant')->table('creditos')
                ->join('clientes', 'creditos.id_cliente', '=', 'clientes.id')
                ->select('creditos.*', 'clientes.nro_doc', 'clientes.nombres')
                ->where('creditos.estado',"=","A")
                ->orderBy('id', 'desc')
                ->paginate($perPage);
        }

        return view('pos/creditos.index', compact('creditos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = Cliente::all();
        $productos = Producto::where('tipo', 'P')->get();
        return view('pos/creditos.create', compact('clientes', 'productos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //var_dump($request->all());exit;
        $post    = $request->all();
        $validator = Validator::make($request->all(), [
            "monto" => "required",
            "id_cliente" => "required",
            "cantidad" => "required"
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($post);
        }
        $productos = $post["cantidad"];
        $descuento = $post["descuento"];
        $codigo = $post["codigo"];
        $credito = new Credit();
        $credito->id_cliente = $post['id_cliente'];
        $credito->monto = $post['monto'];
        $credito->saldo = $post['monto'];
        $credito->estado = "A";
        $credito->save();
        for($i=0;$i<count($productos);$i++){
            $credito_producto = new CreditoProducto();
            $credito_producto->id_credito = $credito->id;
            $credito_producto->id_producto = $codigo[$i];
            $credito_producto->cantidad = $productos[$i];
            $credito_producto->descuento = $descuento[$i];
            $credito_producto->estado = "A";
            $credito_producto->save();
        }       
        return redirect('pos/creditos')->with('success', 'El Crédito fue creado correctamente');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $credito=Credit::find($id);
        $credito_productos = DB::connection('tenant')->table('creditos_producto')
                ->join('productos', 'productos.id', '=', 'creditos_producto.id_producto')
                ->select('creditos_producto.*', 'productos.descripcion', 'productos.codigo')                
                ->where('creditos_producto.id_credito',$id)
                ->orderBy('id', 'desc')
                ->get();
        $clientes=Cliente::where('id',$credito->id_cliente)->get();
        $productos=Producto::where('tipo','P')->get();
        return view('pos.creditos/edit',compact('credito','clientes','productos','credito_productos'));
    }
    public function exportar(Request $request)
    {
        $perPage = 10;

        $creditos = DB::connection('tenant')->table('creditos')
            ->join('clientes', 'creditos.id_cliente', '=', 'clientes.id')
            ->select('creditos.id_cliente', 'creditos.id', 'clientes.nro_doc', 'clientes.nombres')
            ->orderBy('id', 'desc')->get();
        return Excel::download(new CreditosExport($creditos), 'creditos.xlsx');
    }

    public function update(Request $request, $id)
    {
        $post    = $request->all();
        $validator = Validator::make($request->all(), [
            "monto" => "required",
            "id_cliente" => "required",
            "cantidad" => "required"
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($post);
        }
        $productos = $post["cantidad"];
        $descuento = $post["descuento"];
        $codigo = $post["codigo"];
        $credito_producto_id=$post["credito_producto_id"];
        
        $credito = Credit::find($id);
        $credito->id_cliente = $post['id_cliente'];
        $credito->monto = $post['monto'];
        $credito->saldo = $post['saldo'];
        $credito->save();
        for($i=0;$i<count($productos);$i++){
            $credito_producto = CreditoProducto::find($credito_producto_id[$i]);
            $credito_producto->id_credito = $credito->id;
            $credito_producto->id_producto = $codigo[$i];
            $credito_producto->cantidad = $productos[$i];
            $credito_producto->descuento = $descuento[$i];
            $credito_producto->estado = "A";
            $credito_producto->save();
        }       
        return redirect('pos/creditos')->with('success', 'El Crédito fue actualizado correctamente');
    }

    public function destroy($id)
    {
        $credito=Credit::find($id);
        $credito->estado="N";
        $credito->save();
        return redirect('pos/creditos')->with('success', 'El Crédito fue actualizado correctamente');
    }

    public function credito_cliente($id_cliente)
    {
        $creditos = Credit::where("id_cliente", "=", $id_cliente)->get();

        return $creditos;

    }
}
 
