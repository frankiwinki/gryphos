<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Canje;
use App\Producto;
use App\CanjeProductos;
use App\VentaHistorico;
use App\VentaHistoricoDetalle;
use App\ReporteVentaDetalle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CanjesExport;
use App\Exports\ReporteVentasCanjeExport;

class CanjesProductosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            PosController::ChangeDB();
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $keyword = $request->get('search');        
    
        if (!empty($keyword)) {
            $keyword = strtoupper($request->all()["search"]);
            $canjesProductos= CanjeProductos::select('*')
                ->join('productos','canjes_productos.id_producto', 'productos.id')
            ->where("productos.descripcion", "ilike", "%" . $keyword . "%")
            ->orwhere("canjes_productos.id",$keyword)
            ->paginate(25);  
        } else {
            $canjesProductos= CanjeProductos::paginate(25);
        }       
     
        return view('pos.canjes_productos/index',compact('canjesProductos'));
    }
    /** 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productos=Producto::where("tipo","P")->get();
        return view('pos.canjes_productos/create',compact('productos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $canjeProductos_id = CanjeProductos::latest()->first()->id;
        $data=$request->all();
        $canjeProductos= new CanjeProductos();
        // $canjeProductos->id = $canjeProductos_id;
        $canjeProductos->id_producto = $data["id_producto"];
        $canjeProductos->monto_canje = $data["monto_canje"];
        $canjeProductos->meta_canje = $data["meta_canje"];
        $canjeProductos->estado ='A';
        $canjeProductos->save();
        return redirect("/pos/canjesproductos");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productos=Producto::where("tipo","P")->get();
        $canjesProductos= CanjeProductos::find($id);
      
        return view('pos.canjes_productos/edit',compact('canjesProductos','productos'));

    } 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

  
        $data=$request->all();
        $canjeProductos=  CanjeProductos::find($id);
        $canjeProductos->id_producto=$data["id_producto"];
        $canjeProductos->monto_canje=$data["monto_canje"];
        $canjeProductos->meta_canje = $data["meta_canje"];
        $canjeProductos->estado = $data["estado"];
        $canjeProductos->save();
        
        return redirect("/pos/canjesproductos");
    }

    public function destroy($id)
    {
        //
    }
    public function despacho(){
        return view("pos.canjes/despacho");
    }
    public function cliente($num_doc){
        $canje=Canje::where("id_cliente",$num_doc)->first();
        if($canje==null){
            return null;
        }
        return $canje;
    }
    public function detalle($id)
    {
        $id=$id;
        $canjeProducto= CanjeProductos::findOrFail($id);        
        return view("pos.canjes_productos/detalle",compact('canjeProducto','id'));
    }
    public function detalles_exportar($id){
        $ventas=ReporteVentaDetalle::where("ruc",$id)->get();        
        return Excel::download(new ReporteVentasCanjeExport($ventas), 'ventas.xlsx');
    }
    public function detalleItems($id){

        $ventas_detalles = DB::connection('tenant')->table('ventas_detalle_historico')
                ->join('productos', 'productos.id', '=', 'ventas_detalle_historico.id_producto')
                ->select('ventas_detalle_historico.*', 'productos.descripcion', 'productos.codigo')                
                ->where('ventas_detalle_historico.id_venta',$id)
                ->orderBy('id', 'desc')
                ->get();              
        return $ventas_detalles;
        
    }
    public function exportar(Request $request){

        $keyword = $request->get('search');        
    
        if (!empty($keyword)) {
            $keyword = strtoupper($request->all()["search"]);
            
            $clientes=DB::connection('tenant')->table('clientes_consumos')
            ->join('productos', 'productos.id', '=', 'clientes_consumos.id_producto')
            ->select('clientes_consumos.id_cliente','clientes_consumos.nombres','productos.descripcion','clientes_consumos.consumo','clientes_consumos.puntos_usados','clientes_consumos.puntos_activos')
            ->where("nombres", "ilike", "%" . $keyword . "%")
            ->orwhere("id_cliente",$keyword)->get();            

        } else {
            $clientes=DB::connection('tenant')->table('clientes_consumos')
            ->join('productos', 'productos.id', '=', 'clientes_consumos.id_producto')
            ->select('clientes_consumos.id_cliente','clientes_consumos.nombres','productos.descripcion','clientes_consumos.consumo','clientes_consumos.puntos_usados','clientes_consumos.puntos_activos')
            ->get();
        }               
        return Excel::download(new CanjesExport($clientes), 'clientes.xlsx');
    }
}
