<?php

namespace App\Http\Controllers;
use Hash;
use App\UserPos;
use Illuminate\Http\Request;

class UserPosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            PosController::ChangeDB();
            return $next($request);
         });
    }

    public function index()
    {        
        $usuarios=UserPos::all();        
        return view('pos/users.index',compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pos/users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [			
			'usuario' => 'required',
			'password' => 'required',			
        ]);        
        $requestData = $request->all();         
        $requestData['created_at']=date('Y-m-d');        
        UserPos::create($requestData);
        return redirect('pos/users')->with('flash_message', 'Usario agregado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  
        
        $user=UserPos::find($id);  
                     
        return view('pos/users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [			
			'usuario' => 'required',
			'password' => 'required',			
        ]);        
        $requestData = $request->all();            
        $requestData['updated_at']=date('Y-m-d');        
        $user = UserPos::findOrFail($id);
        $user->update($requestData);
        return redirect('pos/users')->with('flash_message', 'Usario actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {                
        $user = UserPos::findOrFail($id);
        $user->status=0;
        $user->update();
        return redirect('pos/users')->with('flash_message', 'Usario deshabilitado!');
    }
}
