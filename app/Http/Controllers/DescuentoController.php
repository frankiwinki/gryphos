<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Producto;
use App\DescuentoProducto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DescuentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            PosController::ChangeDB();
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $query = Cliente::whereRaw('1=1');
        $search="";
        if($request->has('search')){
            $search=$request->search;
           $query->where('nombres','ilike','%'.$request->search.'%');
        }
        $clientes=$query->get();
        return view('pos.descuentos/index', compact('clientes','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function ver($id_cliente)
    {
        $clientes = Cliente::where("id", $id_cliente)->get();

        $descuentos = DescuentoProducto::where("id_cliente", $id_cliente)->get();
        if (count($descuentos) > 0) {
            $productos = DB::connection('tenant')->table('productos')
                ->leftjoin('clientes_producto', 'clientes_producto.id_producto', '=', 'productos.id')
                ->select('productos.*', 'clientes_producto.descuento')
                ->where('tipo', 'P')
                ->where('clientes_producto.id_cliente', $id_cliente)
                ->orderBy('id', 'desc')->get();
        } else {
            $productos = Producto::where("tipo", "P")->get();
        }
        return view("pos.descuentos/show", compact('clientes', 'productos'));
    }
    public function store(Request $request)
    {
        $post = $request->all();
        $obj = DescuentoProducto::where("id_cliente", $post["id_cliente"])->get();
        for ($i = 0; $i < count($post["productos"]); $i++) {
            if (count($obj) > 0) {
                $descuento=DescuentoProducto::find($obj[$i]->id);
            } else {
                $descuento = new DescuentoProducto();
            }
            $descuento->id_cliente = $post["id_cliente"];
            $descuento->estado = "A";
            $descuento->id_producto = $post["productos"][$i];
            $descuento->descuento = $post["descuento"][$i];
            $descuento->save();
        }
        return redirect("/pos/descuentos");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
