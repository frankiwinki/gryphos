<?php

namespace App\Http\Controllers;

use App\Exports\ReporteVentasDetalleExport;
use App\Exports\VentasExport;
use App\Exports\VentasCombustibleExport;
use Illuminate\Http\Request;
use DB;
use App\VentaHistorico;
use App\VentaHistoricoReporte;
use App\User;
use Maatwebsite\Excel\Facades\Excel;
use App\VentaCombustible;
use App\ReporteVentaDetalle;

class ReporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            PosController::ChangeDB();
            return $next($request);
        });
    }

    public function index()
    {
        $ventas = DB::connection('tenant')->select('select * from ventas');
        return view('reporte.index', compact('ventas'));
    }
    public function ventasdetalle(Request $request)
    {
        $perPage = 20;
        $fecha_inicio = "";
        $fecha_fin = "";
        $id_pos = "";
        $tipo_doc = "";
        if ($request->get('id_pos') != null) {
            $id_pos = $request->get('id_pos');
        }
        if ($request->get('tipo_doc') != null) {
            $tipo_doc = $request->get('tipo_doc');
        }
        if ($request->get('fecha_inicio') != null) {
            $fecha_inicio = $request->get('fecha_inicio');
        }
        if ($request->get('fecha_fin') != null) {
            $fecha_fin = $request->get('fecha_fin');
        }
        if ($fecha_inicio != "" && $fecha_fin != "") {
            $query = ReporteVentaDetalle::whereBetween('fecha', array($fecha_inicio, $fecha_fin));
            if ($request->get("tipo_doc") != "0") {
                $query->where("tipo_doc", $request->get("tipo_doc"));
            }
            if ($request->get("id_pos") != "0") {
                $query->where("id_pos", $request->get("id_pos"));
            }
            $query->orderBy('fecha', 'asc');
            $query->orderBy('numero_de_serie', 'asc');
            $ventas = $query->paginate($perPage);
        } else {
            $ventas = null;
        }
        return view('reporte.ventasdetalle', compact('ventas', 'tipo_doc', 'fecha_inicio', 'fecha_fin', 'id_pos'));
    }
    public function buscarventas(Request $request)
    {
        $perPage = 20;
        $fecha_inicio = "";
        $fecha_fin = "";
        $id_pos = "";
        $tipo_doc = "";
       // return $request;
        if ($request->get('id_pos') != null) {
            $id_pos = $request->get('id_pos');
        }
        if ($request->get('tipo_doc') != null) {
            $tipo_doc = $request->get('tipo_doc');
        }
        if ($request->get('fecha_inicio') != null) {
            $fecha_inicio = $request->get('fecha_inicio');
        }
        if ($request->get('fecha_fin') != null) {
            $fecha_fin = $request->get('fecha_fin');
        }
        if ($fecha_inicio != "" && $fecha_fin != "") {
            $query = VentaHistoricoReporte::whereBetween('fecha', array($fecha_inicio, $fecha_fin));
            if ($request->get("tipo_doc") != "0") {
                $query->where("tipo_doc", $request->get("tipo_doc"));
            }
            if ($request->get("id_pos") != "0") {
                $query->where("id_pos", $request->get("id_pos"));
            }
            $query->orderBy('fecha', 'asc');
            $query->orderBy('tipo_tabla10', 'desc');
            $query->orderBy('numero_de_serie', 'asc');
            $ventas = $query->paginate($perPage);
            //return $ventas;
        } else {
            $ventas = null;
        }
       
        $cabecera = array(
            "#", "FECHA EMISION", "SERIE", "NUMERO", "RUC", "NOMBRE",
            "NETO", "IGV",  "IMPORTE TOTAL",
        );
        return view('reporte.reporte', compact('ventas', 'cabecera', 'tipo_doc', 'fecha_inicio', 'fecha_fin', 'id_pos'));
    }

    public function exportar(Request $request)
    {
        $fecha_inicio = "";
        $fecha_fin = "";
        $id_pos = "";
        $tipo_doc = "";
        if ($request->get('id_pos') != null) {
            $id_pos = $request->get('id_pos');
        }
        if ($request->get('tipo_doc') != null) {
            $tipo_doc = $request->get('tipo_doc');
        }
        if ($request->get('fecha_inicio') != null) {
            $fecha_inicio = $request->get('fecha_inicio');
        }
        if ($request->get('fecha_fin') != null) {
            $fecha_fin = $request->get('fecha_fin');
        }
        if ($fecha_fin != "" && $fecha_inicio != "") {            
            $query = VentaHistoricoReporte::whereBetween('fecha', array($fecha_inicio, $fecha_fin));
            if ($request->get("tipo_doc") != "0") {
                $query->where("tipo_doc", $request->get("tipo_doc"));
            }
            if ($request->get("id_pos") != "0") {
                $query->where("id_pos", $request->get("id_pos"));
            }
            $query->orderBy('tipo_tabla10', 'desc')->orderBy('fecha', 'asc');
            $ventas_excel=$query->get();
            $v = new VentasExport($ventas_excel);
            return Excel::download($v, 'ventas.xlsx');
        } else {
            return redirect('admin/reportes/buscar');
        }
    }
    public function exportar_detalle(Request $request){
        $perPage = 20;
        $fecha_inicio = "";
        $fecha_fin = "";
        $id_pos = "";
        $tipo_doc = "";
        if ($request->get('id_pos') != null) {
            $id_pos = $request->get('id_pos');
        }
        if ($request->get('tipo_doc') != null) {
            $tipo_doc = $request->get('tipo_doc');
        }
        if ($request->get('fecha_inicio') != null) {
            $fecha_inicio = $request->get('fecha_inicio');
        }
        if ($request->get('fecha_fin') != null) {
            $fecha_fin = $request->get('fecha_fin');
        }
        if ($fecha_inicio != "" && $fecha_fin != "") {
            $query = ReporteVentaDetalle::whereBetween('fecha', array($fecha_inicio, $fecha_fin));
            if ($request->get("tipo_doc") != "0") {
                $query->where("tipo_doc", $request->get("tipo_doc"));
            }
            if ($request->get("id_pos") != "0") {
                $query->where("id_pos", $request->get("id_pos"));
            }
            $query->orderBy('fecha', 'asc');
            $query->orderBy('numero_de_serie', 'asc');
            $ventas = $query->get();
            $v = new ReporteVentasDetalleExport($ventas);
            return Excel::download($v, 'ventas.xlsx');
        } else {
            return redirect('admin/reportes/detalle');
        }

    }
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function combustibles(Request $request)
    {

        $fecha_inicio = "";
        $fecha_fin = "";
        if (!empty($request->all()["fecha_inicio"]) && !empty($request->all()["fecha_fin"])) {
            $fecha_inicio = $request->all()["fecha_inicio"];
            $fecha_fin = $request->all()["fecha_fin"];
            $ventas = VentaCombustible::where("created_at", ">=", $request->all()["fecha_inicio"])
                ->where("created_at", "<=", $request->all()["fecha_fin"])
                ->orderBy("created_at", "ASC")->get();
        } else {
            $ventas = null;
        }
        return view("reporte.combustible", compact('ventas', 'fecha_inicio', 'fecha_fin'));
    }

    public function combustible_exportar(Request $request)
    {
        $fecha_inicio = "";
        $fecha_fin = "";
        if (!empty($request->all()["fecha_inicio"]) && !empty($request->all()["fecha_fin"])) {
            $fecha_inicio = $request->all()["fecha_inicio"];
            $fecha_fin = $request->all()["fecha_fin"];
            $ventas = VentaCombustible::where("created_at", ">=", $request->all()["fecha_inicio"])
                ->where("created_at", "<=", $request->all()["fecha_fin"])
                ->orderBy("created_at", "ASC")->get();
            $v = new VentasCombustibleExport($ventas);
            return Excel::download($v, 'ventas.xlsx');
        } else {
            $ventas = null;
        }
    }
}
