<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Business;
use DB;

class PosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');        
    }

    public static function ChangeDB()
    {        
        $user = auth()->user();        
        $business_id = $user->business_id;
        $business = Business::find($business_id);                        
        $connectionString = explode('|', $business->connection_string);
        /*$connectionString[0]=port;$connectionString[1]=user;$connectionString[2]=password;
        $connectionString[3]=database;*/
        config(['database.connections.tenant.driver' => $business->database_driver]);
        config(['database.connections.tenant.host' => $business->ip]);
        config(['database.connections.tenant.port' => $connectionString[0]]);
        config(['database.connections.tenant.username' => $connectionString[1]]);
        config(['database.connections.tenant.password' => $connectionString[2]]);
        config(['database.connections.tenant.database' => $connectionString[3]]);
        DB::purge('tenant');
        DB::reconnect('tenant');        
    }
}

