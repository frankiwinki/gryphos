<?php

namespace App\Http\Controllers;

use App\Caja;
use App\CajaResumen;
use Illuminate\Http\Request;

class CajaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            PosController::ChangeDB();
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $keyword = $request->get('turno');
        $fecha = $request->get('fecha');

        $this->generar($fecha, $keyword);


        if (!empty($keyword)) {
            $cajas = Caja::where('turno', '=', $keyword)
                ->where('fecha', '=', $fecha)
                ->orderBy('id_pos', 'asc')->get();
            $caja_resumen = CajaResumen::where('turno', '=', $keyword)
                ->where('fecha', '=', $fecha)
                ->first();
        } else {
            $caja_resumen=null;
            $cajas = null;
        }

        return view("pos/caja.index", compact('cajas','caja_resumen','fecha'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Caja::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $caja = Caja::find($id);
        $post = $request->all();
        $caja->total_combustibles = $post["total_combustible"];
        $caja->total_depositos = $post["total_deposito"];
        $caja->total_creditos = $post["total_credito"];
        $caja->total_otros = $post["total_otro"];
        $caja->descuentos = $post["descuentos"];
        $caja->canjes = $post["canjes"];
        $caja->egresos = $post["egresos"];
        $caja->dev_tanque = $post["dev_tanque"];
        $caja->total_tarjetas = $post["total_tarjeta"];
        $caja->save();
        
        return $caja;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function generar($fecha, $turno)
    {
        $caja_resumen = CajaResumen::where('fecha', '=', $fecha)->where('turno', '=', $turno)->first();
        if ($caja_resumen == null) {
            $caja_resumen = new CajaResumen();
        }
        $cajas = Caja::where('fecha', '=', $fecha)->where('turno', '=', $turno)->get();
        $bruto = 0;
        $creditos = 0;
        $tarjetas = 0;
        $descuentos = 0;
        $devolucion = 0;
        $egreso = 0;
        $efectivo = 0;
        foreach ($cajas as $caja) {
            $bruto = $bruto + $caja->total_combustibles;
            $creditos = $creditos + $caja->total_creditos;
            $tarjetas = $tarjetas + $caja->total_tarjetas;
            $descuentos = $descuentos + $caja->descuentos;
            $devolucion = $devolucion + $caja->dev_tanque;
            $egreso = $egreso + $caja->egresos;
            $efectivo = $efectivo + $caja->total_combustibles - ($caja->total_depositos + $caja->total_creditos + $caja->descuentos + $caja->canjes)
                + $caja->total_otros;
        }
        $caja_resumen->fecha = $fecha;
        $caja_resumen->turno = $turno;
        $caja_resumen->total_combustibles = $bruto;
        $caja_resumen->total_creditos = $creditos;
        $caja_resumen->descuentos = $descuentos;
        $caja_resumen->egresos = $egreso;
        $caja_resumen->dev_tanque = $devolucion;
        $caja_resumen->total_tarjetas = $tarjetas;
        $caja_resumen->efectivo = $efectivo;
        $caja_resumen->save();
    }
    public function resumen(){
        $caja_resumen = new CajaResumen();
    }
}
