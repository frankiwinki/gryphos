<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Credit;
use App\Producto;
use App\Banco;
use App\Vale;
use App\ValeDetalle;
use App\CreditoPago;
use App\CreditoPagoFactura;

use App\TipoPago;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ValesExport;

class ValeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            PosController::ChangeDB();
            return $next($request);
        });
    }

    public function index($id_credito, Request $request)
    {
        $id_producto = "0";
        $fecha_actual = date('Y-m-d');
        $fecha_ini = date('Y-m-d', strtotime($fecha_actual . "-1 month"));
        $fecha_fin = date('Y-m-d');

        if ($request->get('id_producto') != null) {
            $id_producto = $request->get('id_producto');
        }
        if ($request->get('fecha_ini') != null) {
            $fecha_ini = $request->get('fecha_ini');
        }
        if ($request->get('fecha_fin') != null) {
            $fecha_fin = $request->get('fecha_fin');
        }
        $where_producto = "";
        $perPage = 10;
        $productos = Producto::where("tipo", "=", "P")->get();
        $pagos = CreditoPago::where("id_credito", "=", $id_credito)->get();

        if ($fecha_ini != "" && $fecha_fin != "") {

            $query = DB::connection('tenant')->table('creditos_vale')
                ->join('creditos', 'creditos.id', '=', 'creditos_vale.id_credito')
                ->join('clientes', 'creditos.id_cliente', '=', 'clientes.id')
                ->join('creditos_vale_detalle', 'creditos_vale_detalle.id_vale', '=', 'creditos_vale.id')
                ->select(
                    'creditos_vale.id',
                    'creditos_vale.numero_vale',
                    'creditos_vale.id_credito',
                    'creditos_vale.estado',
                    'creditos_vale.created_at',
                    'creditos_vale.id',
                    'creditos_vale_detalle.producto',
                    'creditos_vale_detalle.cantidad',
                    'creditos_vale_detalle.monto',
                    'creditos_vale.fecha_uso',
                    'clientes.nro_doc',
                    'clientes.nombres'
                )
                ->where('creditos_vale.id_credito', '=', $id_credito)
                ->where("creditos_vale.fecha_uso", ">=", $fecha_ini)
                ->where("creditos_vale.fecha_uso", "<=", $fecha_fin)
                ->orderBy('id', 'desc');
            if ($id_producto != 0) {
                $query->where("creditos_vale_detalle.id_producto", "=", $id_producto);
            }
            $vales = $query->paginate($perPage);
        }
        //echo $donde_estoy;exit;
        return view('pos/vales.index', compact('pagos', 'vales', 'id_producto','id_credito', 'productos', 'fecha_ini', 'fecha_fin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_credito)
    {

        $bancos = Banco::all();
        $tipopago = TipoPago::all(); 
        $creditos = Credit::find($id_credito);
        $clientes = Cliente::find($creditos->id_cliente);
        return view('pos/vales.create', compact('creditos', 'clientes', 'bancos', 'tipopago'));
    }


    public function editarpago($id_credito)
    {

        $bancos = Banco::all();
        $tipopago = TipoPago::all();
        $creditos = CreditoPago::find($id_credito);
        $creditostabla = Credit::find($creditos->id_credito);
        $clientes = Cliente::find($creditostabla->id_cliente);

        $pagosFactura = CreditoPagoFactura::where('id_pago', '=', $creditos->id)->get();
        $detalle = ""; 
    
        $variable=0;
        foreach ($pagosFactura as $row) {
            $detalle = $detalle . "<div class='form-group row mb-0 contador_pagos' id='div_pagos_".$variable."' >
                    <div class='col-md-4'>
                        <label for='monto' class='col-form-label text-md-right'> Número de Factura </label>
                        <input  type='text' class='form-control' name='factura[]' value='".$row->num_doc. "'  required >
                    </div>
                    <div class='col-md-3'>
                        <label for='monto' class='col-form-label text-md-right'> Monto </label>
                        <input  type='text' class='form-control' name='monto_factura[]' value='" . $row->monto . "' required  >
                    </div>
                    <div class='col-md-3' style='margin-top: 26px;'>                                        
                        <a  class='btn btn-sm btn-danger' ><i class='fa fa-times-circle' aria-hidden='true' onclick='eliminarPagos(". $variable.",". $row->id.");'></i></a>
                    </div>
                    </div>"; 
        } 

        return view('pos/vales.editpago', compact('creditos', 'clientes', 'bancos', 'tipopago', 'detalle'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post    = $request->all();
        $validator = Validator::make($request->all(), [
            "id_cliente" => "required",
            "id_credito" => "required",
            "serie" => "required",
            "correlativo_inicio" => "required",
            "correlativo_fin" => "required"
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($post);
        }

        for ($i = $post["correlativo_inicio"], $j = 0; $j < ($post["correlativo_fin"] - $post["correlativo_inicio"]); $j++, $i++) {
            $vale = new Vale();
            $vale->id_credito = $post["id_credito"];
            $vale->numero_vale = $post["serie"] . $i;
            $vale->estado = "A";
            $vale->save();
        }
        return redirect('pos/vales')->with('success', 'El Crédito fue creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        
        $cabecera = "<table class='table'><tr class='bg-primary'><th>Código</th><th>Producto</th>
        <th>Cantidad</th><th>Precio</th><th>Usuario</th></tr>";
        $vales = ValeDetalle::where('id_vale', '=', $id)->get();
        $detalle = "";
        foreach ($vales as $row) {
            $detalle = $detalle . "<tr><td>" . $row->id_producto . "</td><td>" . $row->producto . "</td>" .
                "<td>" . $row->cantidad . "</td><td>" . $row->ppu . "</td><td>" . $row->monto . "</td></tr>";
        }
        return $cabecera . $detalle;
    }

    public function infoPagos($id)
    {

       

        $p = CreditoPago::find($id);

        $banco=self::bancoName($p->banco);
        $tipopago = self::tipopagoName($p->tipo_pago);
        $cliente = self::nameClient($id);
        
   
        $cabecera = " <div class='form-group row'>
                    <div class='col-md-6'>
                        <label for='cliente' class='col-form-label text-md-right'> <b>Cliente : </label>
                         <label for='cliente' class='col-form-label text-md-right'> ". $cliente. "</label>
                    </div>
                    <div class='col-md-4'>
                         <label for='nro_credito' class='col-form-label text-md-right'> <b>N° Crédito : </label>
                         <label for='nro_credito' class='col-form-label text-md-right'> ". $p->id_credito. " </label>
                    </div>
                </div>

                 <div class='form-group row'>
                    <div class='col-md-6'>
                        <label for='banco' class='col-form-label text-md-right'> <b>Banco : </label>
                         <label for='banco' class='col-form-label text-md-right'> " . $banco . " </label>
                    </div>
                    <div class='col-md-4'>
                         <label for='tipo_pago' class='col-form-label text-md-right'> <b>Tipo de Pago : </label>
                         <label for='tipo_pago' class='col-form-label text-md-right'> " . $tipopago . " </label>
                    </div>
                </div>

                <div class='form-group row'>
                    <div class='col-md-6'>
                        <label for='nro_orden' class='col-form-label text-md-right'>  <b> N° de Operación : </b> </label>
                         <label for='nro_orden' class='col-form-label text-md-right'> " . $p->nro_operacion . " </label>
                    </div>
                    <div class='col-md-4'>
                         <label for='monto' class='col-form-label text-md-right'>  <b>Monto : </b> </label>
                         <label for='monto' class='col-form-label text-md-right'> " . $p->monto. " </label>
                    </div>
                </div>
                <br>
                FACTURAS ASOCIADOS AL PAGO
                <hr>
                ";
        $cabecera .= "<table class='table'><tr class='bg-primary'><th>Código</th>
        <th>N° Factura</th><th>Monto</th></tr>";
        $vales = CreditoPagoFactura::where('id_pago', '=', $id)->get();
        $detalle = "";
        foreach ($vales as $row) {
            $detalle = $detalle . "<tr><td>" . $row->id . "</td><td>". $row->num_doc . "</td><td>" . $row->monto . "</td></tr>";
        }
        return $cabecera . $detalle;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bancos = Banco::all();
        $vale=Vale::find($id);
        $vale_detalle=ValeDetalle::where("id_vale",$id)->get();
        $creditos = Credit::find($vale->id_credito);
        $clientes = Cliente::find($creditos->id_cliente);
        return view("pos.vales/edit", compact('vale','creditos', 'clientes', 'bancos'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {

        $arrayFactura = $request->factura;
        $arrayMonto = $request->monto_factura;

        $creditoPago = new CreditoPago();
        $creditoPago->banco = $request->banco;
        $creditoPago->nro_operacion = $request->nro_operacion;
        $creditoPago->tipo_pago = $request->tipo_pago;
        $creditoPago->monto = $request->monto;
        $creditoPago->estado = 'A';
        $creditoPago->id_credito = $request->id_credito;
        $creditoPago->save();

        $id_pago = $creditoPago->id;
        $arrayFactura=$request->factura;
        $arrayMonto = $request->monto_factura;
        
        

        if($arrayFactura!=""){

            for ($i = 0; $i < count($arrayFactura); $i++) {

                $creditoPagoFactura = new CreditoPagoFactura();
                $creditoPagoFactura->id_pago = $id_pago;
                $creditoPagoFactura->num_doc = $arrayFactura[$i];
                $creditoPagoFactura->monto =  $arrayMonto[$i];
                $creditoPagoFactura->save();
            }


        }
 
    

        $idCredito = $request->id_credito;
        return redirect('pos/vales/listar/'. $idCredito.'');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public static function bancoName($id)
    {
        $p = Banco::find($id);
        return $p->descripcion;
    }

    public static function tipopagoName($id)
    {
        $p = TipoPago::find($id);
        return $p->descripcion;
    }
 
    public static function nameClient($id)
    {
        $p = Credit::find($id);
        $client = Cliente::find($p->id_cliente);
        return $client->nombres;
    }


    public static function facturasPagoCreditos($id)
    {
        $p = CreditoPagoFactura::where("id_pago","=",$id)->get();
        $facturas="";
        foreach($p as $data){
        $facturas.=","." ".$data->num_doc;
        }

        if($facturas!=""){

            $facturas=substr($facturas, 1);
        }
      
        return $facturas;
    }


    public function destroy($id)
    {
        //
    }
    public function exportar(Request $request)
    {
        $id_credito = $request->get('id_credito');
        $id_producto = "0";
        $fecha_actual = date('Y-m-d');
        $fecha_ini = date('Y-m-d', strtotime($fecha_actual . "-1 month"));
        $fecha_fin = date('Y-m-d');
        
        if ($request->get('id_producto') != null) {
            $id_producto = $request->get('id_producto');
        }
        if ($request->get('fecha_ini') != null) {
            $fecha_ini = $request->get('fecha_ini');
        }
        if ($request->get('fecha_fin') != null) {
            $fecha_fin = $request->get('fecha_fin');
        }
        $where_producto = "";
        $perPage = 10;           

        if ($fecha_ini != "" && $fecha_fin != "") {

            $query = DB::connection('tenant')->table('creditos_vale')
                ->join('creditos', 'creditos.id', '=', 'creditos_vale.id_credito')
                ->join('clientes', 'creditos.id_cliente', '=', 'clientes.id')
                ->join('creditos_vale_detalle', 'creditos_vale_detalle.id_vale', '=', 'creditos_vale.id')
                ->select(
                    'creditos_vale.fecha_uso',
                    'clientes.id',
                    'creditos_vale.id_credito',  
                    'clientes.nro_doc',
                    'clientes.nombres',
                    'creditos_vale.numero_vale',                                                                              
                    'creditos_vale_detalle.producto',
                    'creditos_vale_detalle.cantidad',
                    'creditos_vale_detalle.monto'                                     
                )
                ->where('creditos_vale.id_credito', '=', $id_credito)
                ->where("creditos_vale.fecha_uso", ">=", $fecha_ini)
                ->where("creditos_vale.fecha_uso", "<=", $fecha_fin)
                ->orderBy('id', 'desc');
            if ($id_producto != 0) {
                $query->where("creditos_vale_detalle.id_producto", "=", $id_producto);
            }
            $vales = $query->get();
        }
        return Excel::download(new ValesExport($vales), 'vales.xlsx');
    }
}
