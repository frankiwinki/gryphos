<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Producto;
use App\Business;
use App\Tank;
use DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            self::ChangeDB();
            return $next($request);
         });
    }
    
    public function index()
    {
        $tanks=Tank::all();        
        return view('admin.dashboard',compact('tanks'));
    }

    public static function ChangeDB()
    {
        $user = auth()->user();        
        $business_id = $user->business_id;
        $business = Business::find($business_id);                        
        $connectionString = explode('|', $business->connection_string);
        /*$connectionString[0]=port;$connectionString[1]=user;$connectionString[2]=password;
        $connectionString[3]=database;*/
        config(['database.connections.tenant.driver' => $business->database_driver]);
        config(['database.connections.tenant.host' => $business->ip]);
        config(['database.connections.tenant.port' => $connectionString[0]]);
        config(['database.connections.tenant.username' => $connectionString[1]]);
        config(['database.connections.tenant.password' => $connectionString[2]]);
        config(['database.connections.tenant.database' => $connectionString[3]]);
        DB::purge('tenant');
        DB::reconnect('tenant');        
    }
}
