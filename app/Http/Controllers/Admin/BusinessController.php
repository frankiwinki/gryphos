<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Business;
use Illuminate\Http\Request;

class BusinessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $business = Business::where('id', 'LIKE', "%$keyword%")
                ->orWhere('business_name', 'LIKE', "%$keyword%")
                ->orWhere('business_ruc', 'LIKE', "%$keyword%")
                ->orWhere('ip', 'LIKE', "%$keyword%")
                ->orWhere('connection_string', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $business = Business::latest()->paginate($perPage);
        }

        return view('business.index', compact('business'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('business.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'id' => 'required',
			'business_name' => 'required',
            'business_ruc' => 'required',
            'database_driver' => 'required',
			'ip' => 'required',
			'connection_string' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        Business::create($requestData);

        return redirect('admin/business')->with('flash_message', 'Business added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $business = Business::findOrFail($id);

        return view('business.show', compact('business'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $business = Business::findOrFail($id);

        return view('business.edit', compact('business'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'id' => 'required',
			'business_name' => 'required',
            'business_ruc' => 'required',
            'database_driver' => 'required',
			'ip' => 'required',
			'connection_string' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        $business = Business::findOrFail($id);
        $business->update($requestData);

        return redirect('admin/business')->with('flash_message', 'Business updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Business::destroy($id);

        return redirect('admin/business')->with('flash_message', 'Business deleted!');
    }
}
