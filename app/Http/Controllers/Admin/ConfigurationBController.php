<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PosController;

use App\Business;
use App\ConfigurationB;
use DB;

use Illuminate\Http\Request;

class ConfigurationBController extends Controller
{
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
 

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            PosController::ChangeDB();
            return $next($request);
        });
    }

    public function index(Request $request)
    {

       $configurations = DB::connection('tenant')->select('select * from configuracion');
       

        return view('configurationB.index', compact('configurations'));
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('business.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'id' => 'required',
			'business_name' => 'required',
            'business_ruc' => 'required',
            'database_driver' => 'required',
			'ip' => 'required',
			'connection_string' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        Business::create($requestData);

        return redirect('admin/business')->with('flash_message', 'Business added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $configurations = ConfigurationB::findOrFail($id);

        // $configurations = DB::connection('tenant')->select('select * from configuracion where id=' . $id);

        return view('configurationB.show', compact('configurations'));

        // return view('business.show', compact('business'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */ 
    public function edit($id)
    {
        $configurations = ConfigurationB::findOrFail($id);

        // $count = count($configurations);
        // $arrayNuevo = array();
        // // print_r($configurations);
        // // exit;
     
        // for ($i = 0; $i < $count; $i++) {

        //     $impresion_automatica="NO";
        //     $seguridad_formulario = "NO";
        //     $canjes = "NO";
        //     $control_caja = "NO";
          
        //     if(!$configurations[$i]['impresion_automatica']==""){
        //         $impresion_automatica="SI";
        //     }

        //     if (!$configurations[$i]['seguridad_formulario'] == "") {
        //         $seguridad_formulario = "SI";
        //     }

        //     if (!$configurations[$i]['meta_canje'] == "") {
        //         $meta_canje = "SI";
        //     }

        //     if (!$configurations[$i]['canjes'] == "") {
        //         $canjes = "SI";
        //     }

        //     if (!$configurations[$i]['control_caja'] == "") {
        //         $control_caja = "SI";
        //     }

        //     $array = array(
        //         "id" => $configurations[$i]['id'],
        //         "impresion_automatica" => $impresion_automatica,
        //         "seguridad_formulario"   => $seguridad_formulario,
        //         "canjes"  => $canjes,
        //         "meta_canje" => $configurations[$i]['meta_canje'],
        //         "control_caja"  => $control_caja,
        //         "producto_canje" => $configurations[$i]['producto_canje'],
        //         "monto_canje"=> $configurations[$i]['monto_canje']
        //     );

        //     array_push($arrayNuevo, $array);
        // }
        // $configurations= $arrayNuevo;
        // print_r(compact('configurations'));
        // exit;
        return view('configurationB.edit', compact('configurations'));

    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'id' => 'required',
            // 'impresion_automatica' => 'required',
            // 'seguridad_formulario' => 'required',
            // 'canjes' => 'required',
            'meta_canje' => 'required',
            // 'control_caja' => 'required',
            'producto_canje' => 'required' ,
            'monto_canje' => 'required', 
		]);
        $requestData = $request->all();

        $configurations = ConfigurationB::findOrFail($id);
        $configurations->update($requestData);

        return redirect('admin/configurationB')->with('flash_message', 'Business updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Business::destroy($id);

        return redirect('admin/business')->with('flash_message', 'Business deleted!');
    }
}
