<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClientePosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {        
        $this->middleware(function ($request, $next) {
            PosController::ChangeDB();
            return $next($request);
         });
    }
    
    public function index(Request $request)
    {                
        $keyword = $request->get('search');        
        $perPage = 10;
        if (!empty($keyword)) {
            $clientes = Cliente::where('nombres', 'ILIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $clientes = Cliente::latest()->paginate($perPage);
        }
        return view("pos/clientes.index",compact('clientes'));
    }

    public function get_clientes(){
        return Cliente::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("pos/clientes.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post    = $request->all();
        $validator = Validator::make($request->all(), [
            "nro_doc" => "required",
            "nombres"=>"required",  
            ]);
        if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator)
                ->withInput($post);
        }
            Cliente::create($post);  
            return redirect('pos/clientes')->with('success', 'El Cliente fue creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente=Cliente::find($id);
        return view("pos.clientes/edit",compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     $cliente=CLiente::find($id);
     $data=$request->all();
     $cliente->nro_doc=$data["nro_doc"];
     $cliente->nombres=$data["nombres"];
     $cliente->distrito=$data["distrito"];
     $cliente->provincia=$data["provincia"];
     $cliente->departamento=$data["departamento"];
     $cliente->estado=$data["estado"];
     $cliente->save();
     return view('pos.clientes/edit',compact('cliente'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
