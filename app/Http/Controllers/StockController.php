<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tank;
use App\Producto;
use App\Varillaje;
use DB;
use Illuminate\Support\Facades\Session;

class StockController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            PosController::ChangeDB();
            return $next($request);
        });
    }

    public function varillaje()
    {
        $tanks = Tank::all();
        $varillajes = Varillaje::all();
        return view('stock/varillaje', compact('tanks', 'varillajes'));
    }

    public function tanks(Request $request)
    {
        $edit = false;
        $productos = Producto::where("tipo", "P")->get();
        $tanks = Tank::all();
        $tank = new Tank();
        return view('stock/tanks', compact('tanks', 'tank', 'productos', 'edit'));
    }
    public function tanksEdit(Request $request, $id)
    {
        $edit = true;
        $tank = Tank::find($id);
        $productos = Producto::where("tipo", "P")->get();
        $tanks = Tank::all();
        return view('stock/tanks', compact('tanks', 'tank', 'productos', 'edit'));
    }
    public function tanks_store(Request $request)
    {
        $tank = new Tank();
        $data = $request->all();
        $tank->name = $data["name"];
        $tank->id_producto = $data["id_producto"];
        $tank->stock = $data["stock"];
        $tank->save();
        return redirect("/admin/tanks");
    }
    public function tanks_update(Request $request, $id)
    {
        $tank = Tank::find($id);
        $data = $request->all();
        $tank->name = $data["name"];
        $tank->id_producto = $data["id_producto"];
        $tank->stock = $data["stock"];
        $tank->save();
        return redirect("/admin/tanks");
    }

    public function tanks_report()
    {
        $fecha_inicio = "";
        $fecha_fin = "";
        return view('reporte/tanks', compact('fecha_inicio', 'fecha_fin'));
    }

    public static function productName($id)
    {
        $p = Producto::find($id);
        return $p->descripcion;
    }
    public static function tankName($id)
    {
        $t = Tank::find($id);
        return $t->name;
    }
    public function varillaje_store(Request $request)
    {
        DB::beginTransaction();
        try {
            $varillaje = new Varillaje();
            $data = $request->all();
            $varillaje->fecha = $data["fecha"];
            $varillaje->hora = $data["hora"];
            $varillaje->id_tanque = $data["id_tanque"];
            $varillaje->stock = $data["stock"];
            $varillaje->save();
            $tank = Tank::find($data["id_tanque"]);
            $tank->stock = $varillaje->stock;
            $tank->save();
            DB::commit();
            Session::flash('success_message', 'Biography was updated! However, because this is a demo the records are not persisted to the database.');
            return redirect("/admin/varillaje");
        } catch (\Exception $e) {            
            DB::rollBack();
            return $e->getMessage();
        }
    }
}
