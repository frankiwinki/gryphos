<?php

namespace App\Http\Controllers;

use App\Exports\ProductosExport;
use Illuminate\Http\Request;
use DB;
use App\Producto;
use App\User;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use App\Role;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $timestamps = false;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            PosController::ChangeDB();
            return $next($request);
        });
    }

    public function index(Request $request)
    {

        $search = $request->get('search');      

        if (!empty($search)) {
            $search = strtoupper($request->all()["search"]);
        }
        else{
            $search="";
        }
        $productos = Producto::where("descripcion", "like", "%" . $search . "%")->get();
        return view('pos.productos/index', compact('productos'));
    }
    public function buscarventas(Request $request)
    {
        $perPage = 10;
        $fecha_inicio = "";
        $fecha_fin = "";
        if ($request->get('fecha_inicio') != null) {
            $fecha_inicio = $request->get('fecha_inicio');
        }
        if ($request->get('fecha_fin') != null) {
            $fecha_fin = $request->get('fecha_fin');
        }
        if ($fecha_inicio != "" && $fecha_fin != "") {
            $ventas = VentaHistoricoReporte::whereBetween('fecha', array($fecha_inicio, $fecha_fin))->orderBy('fecha', 'asc')->orderBy('tipo_tabla10', 'desc')->paginate($perPage);
        } else {
            $ventas = null;
        }
        $cabecera = array(
            "#", "FECHA EMISION",
            "TIPO (TABLA 10)", "SERIE", "NUMERO", "TIPO (TABLA 2)", "RUC", "NOMBRE",
            "NETO", "IGV",  "IMPORTE TOTAL",
        );
        return view('reporte.reporte', compact('ventas', 'cabecera', 'fecha_inicio', 'fecha_fin'));
    }

    public function create()
    {
        $perPage = 10;
        $productos = Producto::paginate($perPage);
        return view('pos.productos/create', compact('productos'));
    }

    public function store(Request $request)
    {
        $post = $request->all();
        Producto::create($post);
        return redirect('pos/productos')->with('flash_message', 'Product added!');
    }
    public function exportar(Request $request)
    {
        return Excel::download(new ProductosExport, 'ventas.xlsx');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::find($id);
        return view('pos.productos/edit', compact('producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = $request->all();
        $producto = Producto::find($id);
        $producto->cod_barra = $post["cod_barra"];
        $producto->precio = $post["precio"];
        $producto->descripcion = $post["descripcion"];
        $producto->save();
        return redirect('pos/productos')->with('flash_message', 'Product modified!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listar(){
        return Producto::where("tipo","X")->get();
    }
}
