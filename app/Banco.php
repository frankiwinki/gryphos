<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
    protected $connection = 'tenant';
    protected $table = 'bancos';
}
