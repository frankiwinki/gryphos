<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    //
    protected $connection = 'tenant';
    protected $table = 'creditos';

    protected $fillable = [
        'id_cliente','id_credito','monto', 'saldo'
    ];
}
