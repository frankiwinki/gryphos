<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentaHistoricoReporte extends Model
{
    protected $connection = 'tenant';    
    protected $table = 'v_ventahistoricoreporte';
    const CREATED_AT = 'fecha';
    // protected $fillable = [
    //     "total","igv","tipo_doc","placa","id_cliente","serie","correlativo"
    // ];
}
