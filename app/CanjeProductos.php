<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CanjeProductos extends Model
{
    public $timestamps = false;
    protected $connection = 'tenant';
    protected $table = 'canjes_productos';
}
 