<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentaCombustible extends Model
{
    protected $connection = 'tenant';
    //
    protected $table = 'v_ventascombustible';

}
