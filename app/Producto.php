<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $connection = 'tenant';
    protected $table = 'productos';

    protected $fillable = [
        'codigo','descripcion', 'tipo'
    ];
}
