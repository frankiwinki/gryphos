<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class XmlDetalle extends Model
{
    protected $connection = 'tenant';    
    protected $table = 'v_xmldetalle';
}
