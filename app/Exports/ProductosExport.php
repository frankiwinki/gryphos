<?php

namespace App\Exports;

use App\Producto;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductosExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Producto::all();
    }
    public function headings(): array
    {
        return [
            "ID","CODIGO","PRODUCTO","TIPO","COD. BARRA","PRECIO"
        ];
    }
}
