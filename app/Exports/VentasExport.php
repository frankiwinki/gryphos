<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class VentasExport implements FromCollection,WithHeadings,WithCustomCsvSettings
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ',',
            'use_bom'=>true
        ];
    }
    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            "FECHA EMISION","HORA", "FECHA VENCIMIENTO",
            "TIPO (TABLA 10)", "SERIE", "NUMERO", "TIPO (TABLA 2)", "RUC", "NOMBRE",
            "VALOR FACTURADO", "NETO", "EXONERADO", "INAFECTA", "ISC", "IGV", "OTROS TRIBUTOS", "IMPORTE TOTAL"
        ];
    }
    
}
