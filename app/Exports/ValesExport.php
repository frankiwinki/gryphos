<?php

namespace App\Exports;

use App\ValeDetalle;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ValesExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

     public function collection()
    {
        return $this->data;
    }
    public function headings(): array
    {
        return [
            "FECHA","ID_CLIENTE","ID_CREDITO","DOC","NOMBRES","VALE",
            "PRODUCTO","CANTIDAD","MONTO"
        ];
    }
}
