<?php

namespace App\Exports;

use App\ReporteVentaDetalle;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteVentasDetalleExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            "FECHA","HORA","SERIE","CORRELATIVO","RUC/DNI","CLIENTE","PTO","PRODUCTO","-","BASE","IGV","-","TOTAL","TIPO","USUARIO"
        ];
    }
}
