<?php

namespace App\Exports;

use App\Canje;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;


class CanjesExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }
    public function headings(): array
    {
        return [
            "DNI/RUC","NOMBRES","PRODUCTO","CONSUMO TOTAL","PUNTOS USADOS","PUNTOS ACTIVOS"
        ];
    }
}
