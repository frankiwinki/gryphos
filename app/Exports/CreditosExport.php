<?php

namespace App\Exports;

use App\Credit;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class CreditosExport implements FromCollection,WithHeadings
{ 
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            "ID_CLIENTE","ID_CREDITO","RUC","CLIENTE"
        ];
    }
}
