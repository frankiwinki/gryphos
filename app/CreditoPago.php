<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditoPago extends Model
{
    
    protected $connection = 'tenant';
    protected $table = 'creditos_pagos';    
}
