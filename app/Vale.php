<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vale extends Model
{
    protected $connection = 'tenant';
    //
    protected $table = 'creditos_vale';

    protected $fillable = [
        'id_cliente','monto', 'saldo'
    ];
}
