<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Canje extends Model
{
    protected $connection = 'tenant';
    protected $table = 'clientes_consumos';
}
