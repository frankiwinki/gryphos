<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ValeDetalle extends Model
{
    protected $connection = 'tenant';
    //
    protected $table = 'creditos_vale_detalle';

    protected $fillable = [
        'id_producto','producto', 'ppu','monto','id_vale','cantidad'
    ];
}
