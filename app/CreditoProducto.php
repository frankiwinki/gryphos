<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditoProducto extends Model
{
    protected $connection = 'tenant';
    protected $table = 'creditos_producto';

    protected $fillable = [
        'id_credito','id_producto', 'estado','cantidad'
    ];
}
