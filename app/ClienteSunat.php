<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteSunat extends Model
{
    protected $connection = 'tenant';
    protected $table = 'clientes_sunat';   
}
