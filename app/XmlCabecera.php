<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class XmlCabecera extends Model
{
    protected $connection = 'tenant';    
    protected $table = 'v_xmlcabecera';
}
