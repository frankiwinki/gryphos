<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DescuentoProducto extends Model
{
    protected $connection = 'tenant';
    protected $table = 'clientes_producto';
}
