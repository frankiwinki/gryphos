<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caja extends Model
{
    protected $connection = 'tenant';
    protected $table = 'caja_historico';

    protected $fillable = [
        'id_pos','total_combustibles', 'total_depositos', 'total_credito', 'total_otros','turno','fecha'
    ];
}
