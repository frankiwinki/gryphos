<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentaHistoricoDetalle extends Model
{
    protected $connection = 'tenant';    
    protected $table = 'ventas_detalle_historico';
    
}
