<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CajaResumen extends Model
{
    protected $connection = 'tenant';
    protected $table = 'caja_resumen';

    protected $fillable = [
        'total_combustibles', 'total_depositos', 'total_credito', 'total_otros','turno','fecha'
    ];
}

