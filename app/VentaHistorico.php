<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentaHistorico extends Model
{
    protected $connection = 'tenant';    
    protected $table = 'ventas_historico';
    const CREATED_AT = 'fecha';
    protected $fillable = [
        "total","igv","tipo_doc","placa","id_cliente","serie","correlativo"
    ];
}
