@extends('layouts.backend')

@section('content')
<div class="">
    <div class="row">
        @include('admin.sidebar')
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Generar Comprobantes Electrónicos</div>
                <div class="card-body p-2">
                    <h5></h5>
                    <div>
                        <form method="GET" action="/admin/reportes/buscar" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <div class="form-group">
                                    <label for="">Fecha de Inicio </label>
                                    <input type="date" class="form-control" name="fecha_inicio" value="{{ $fecha_inicio }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Fecha de Fin </label>
                                    <input type="date" class="form-control" name="fecha_fin" value="{{ $fecha_fin }}">
                                </div>

                                <span class="input-group">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                                <span class="ml-2 input-group">
                                    <a href="exportar?fecha_inicio={{$fecha_inicio}}&&fecha_fin={{$fecha_fin}}" class="btn btn-success">
                                        <i class="fa fa-code"></i> GENERAR
                                    </a>
                                </span>
                                <span class="ml-2 input-group">
                                    <a href="enviar?fecha_inicio={{$fecha_inicio}}&&fecha_fin={{$fecha_fin}}" class="btn btn-danger">
                                        <i class="fa fa-paper-plane" aria-hidden="true"></i> ENVIAR
                                    </a>
                                </span>

                            </div>
                        </form>
                        <br>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped mt-5">

                            <thead>

                            </thead>

                            <tbody>

                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    // $('#fecha_inicio').mask("99/99/9999", {placeholder: 'MM/DD/YYYY' });
</script>
@endsection