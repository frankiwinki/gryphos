<div class="form-group{{ $errors->has('id') ? 'has-error' : ''}}">
    {!! Form::label('id', 'Id', ['class' => 'control-label']) !!}
    {!! Form::number('id', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('impresion_automatica') ? 'has-error' : ''}}">
    {!! Form::label('impresion_automatica', 'Impresión Automatica', ['class' => 'control-label']) !!}
    {!! Form::select('impresion_automatica', array(''=>'NO','1'=>'SI'),null,['class'=>'form-control']) !!}
    {!! $errors->first('impresion_automatica', '<p class="help-block">:message</p>') !!}
</div>
 
<div class="form-group{{ $errors->has('seguridad_formulario') ? 'has-error' : ''}}">
    {!! Form::label('seguridad_formulario', 'Seguridad Formulario', ['class' => 'control-label']) !!}
    {!! Form::select('seguridad_formulario', array(''=>'NO','1'=>'SI'),null,['class'=>'form-control']) !!}
    {!! $errors->first('seguridad_formulario', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('canjes') ? 'has-error' : ''}}">
    {!! Form::label('canjes', 'Canjes', ['class' => 'control-label']) !!}
    {!! Form::select('canjes', array(''=>'NO','1'=>'SI'),null,['class'=>'form-control']) !!}
    {!! $errors->first('canjes', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('meta_canje') ? 'has-error' : ''}}">
    {!! Form::label('meta_canje', 'Meta de Canje', ['class' => 'control-label']) !!}
    {!! Form::text('meta_canje', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('meta_canje', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('control_caja') ? 'has-error' : ''}}">
    {!! Form::label('control_caja', 'Control de Caja', ['class' => 'control-label']) !!}
    {!! Form::select('control_caja', array(''=>'NO','1'=>'SI'),null,['class'=>'form-control']) !!}
    {!! $errors->first('control_caja', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('producto_canje') ? 'has-error' : ''}}">
    {!! Form::label('producto_canje', 'Producto Canje', ['class' => 'control-label']) !!}
    {!! Form::text('producto_canje', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('producto_canje', '<p class="help-block">:message</p>') !!}
</div>
 
<div class="form-group{{ $errors->has('monto_canje') ? 'has-error' : ''}}">
    {!! Form::label('monto_canje', 'Monto Canje', ['class' => 'control-label']) !!}
    {!! Form::text('monto_canje', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('monto_canje', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('ip_server') ? 'has-error' : ''}}">
    {!! Form::label('ip_server', 'Ip Servidor de Base de Datos', ['class' => 'control-label']) !!}
    {!! Form::text('ip_server', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('ip_server', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('contra_server') ? 'has-error' : ''}}">
    {!! Form::label('contra_server', 'Contraseña de Servidor de Base de Datos', ['class' => 'control-label']) !!}
    {!! Form::text('contra_server', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('contra_server', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('ruc_empresa') ? 'has-error' : ''}}">
    {!! Form::label('ruc_empresa', 'Ruc de Empresa', ['class' => 'control-label']) !!}
    {!! Form::text('ruc_empresa', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('ruc_empresa', '<p class="help-block">:message</p>') !!}
</div>  
<div class="form-group{{ $errors->has('razon_social') ? 'has-error' : ''}}">
    {!! Form::label('razon_social', 'Razon Social', ['class' => 'control-label']) !!}
    {!! Form::text('razon_social', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('razon_social', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('ip_fusion') ? 'has-error' : ''}}">
    {!! Form::label('ip_fusion', 'IP Fusión', ['class' => 'control-label']) !!}
    {!! Form::text('ip_fusion', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('ip_fusion', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('ip_consulta_ruc') ? 'has-error' : ''}}">
    {!! Form::label('ip_consulta_ruc', 'IP Consulta RUC', ['class' => 'control-label']) !!}
    {!! Form::text('ip_consulta_ruc', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('ip_consulta_ruc', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
