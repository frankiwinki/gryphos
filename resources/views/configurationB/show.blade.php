@extends('layouts.backend')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h3 class="page-header">
            Configuración
        </h3>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active"> Configuración </li>
        </ol>

    </div>
    <div id="page-inner">

           <div class="row">
            <div class="col-md-12">
            <a href="/admin/configurationB" class="btn btn-sm btn-success">Visualización de Configuración</a>
            </div>
        </div> 
          
             <div class="card" style="padding: 25px;">
                    <div class="card-header">Configuración {{ $configurations->id }} </div>
                    <div class="card-body">
                                       
                        <a href="{{ url('/admin/configurationB') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás</button></a>
                        <a href="{{ url('/admin/configurationB/' . $configurations->id . '/edit') }}" title="Edit Business"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/configurationB', $configurations->id],
                            'style' => 'display:inline'
                        ]) !!}
                           
                        {!! Form::close() !!}
                        <br/>
                        <br/> 

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $configurations->impresion_automatica }}</td>
                                    </tr>
                                    <tr>
                                        <th> Impresión Automatica </th>
                                            <td> 
                                                @if($configurations->impresion_automatica =='')         
                                                NO       
                                                @else
                                                SI      
                                                @endif
                                            </td>
                                        </tr>
                                        <tr><th> Seguridad Formulario </th>
                                            <td> 
                                             @if($configurations->seguridad_formulario =='')         
                                              NO       
                                            @else
                                                SI     
                                            @endif    
                                            </td>
                                        </tr>
                                        <tr><th> Canjes </th>
                                            <td> 
                                                @if($configurations->canjes =='')         
                                                    NO     
                                                @else
                                                    SI      
                                                @endif
                                            </td>
                                        </tr>
                                         <tr><th> Meta Canjes </th>
                                            <td> {{$configurations->meta_canje}} </td>
                                        </tr>

                                          <tr><th> Control Caja </th>
                                            <td> @if($configurations->control_caja =='')         
                                            NO       
                                        @else
                                            SI      
                                        @endif </td>
                                          </tr>

                                        <tr><th> Monto Canje </th>
                                            <td> {{ $configurations->monto_canje }} </td>
                                        </tr>
                              
                                         <tr><th> IP Servidor de Base de Datos </th>
                                            <td> {{ $configurations->ip_server }} </td>
                                         </tr> 

                                        <tr><th> Contraseña de Base Datos </th>
                                            <td> {{ $configurations->contra_server }} </td>
                                        </tr>

                                        <tr><th> RUC de Empresa </th>
                                            <td> {{ $configurations->ruc_empresa }} </td>
                                        </tr>

                                       <tr><th> Razon Social </th>
                                            <td> {{ $configurations->razon_social }} </td>
                                        </tr>

                                           <tr><th> IP Fusión </th>
                                            <td> {{ $configurations->ip_fusion }} </td>
                                        </tr>

                                           <tr><th> IP Consulta de Ruc </th>
                                            <td> {{ $configurations->ip_consulta_ruc }} </td>
                                        </tr>

                                     
                                         
                                       
                                </tbody>
                            </table>
                        </div>

             
                        </div>

                    </div>
                </div>
    </div>
</div>
<script>
    // $('#fecha_inicio').mask("99/99/9999", {placeholder: 'MM/DD/YYYY' });
</script>
@endsection
 
