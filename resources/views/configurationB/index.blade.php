@extends('layouts.backend')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h3 class="page-header">
            Configuración
        </h3>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active"> Configuración </li>
        </ol>

    </div>
    <div id="page-inner">

           <div class="row">
            <div class="col-md-12">
            <a href="/admin/configurationB" class="btn btn-sm btn-success">Listado de Configuración</a>
            </div>
        </div> 
        
             <div class="card" style="padding: 25px;">
                    <div class="card-header">Configuración</div>
                    <div class="card-body">
                    
                        <br/> 
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Impresión Automatica</th>
                                        <th>Seguridad Formulario</th>
                                        <th>Canjes</th>
                                        <th>Meta Canjes</th>
                                        <th>Producto Canje</th>
                                        <th>Monto Canje</th>
                                        <th>Control Caja</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($configurations as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>
                                        @if($item->impresion_automatica =='')         
                                            NO       
                                        @else
                                            SI      
                                        @endif
                                        </td>
                                       <td>
                                        @if($item->seguridad_formulario =='')         
                                            NO       
                                        @else
                                            SI      
                                        @endif
                                        </td>
                                       <td>
                                        @if($item->canjes =='')         
                                            NO      
                                        @else
                                            SI      
                                        @endif
                                        </td>
                                       <td>{{$item->meta_canje}}</td>

                                        <td>

                                        @if($item->control_caja =='')         
                                            NO       
                                        @else
                                            SI      
                                        @endif
                                        </td>
                                       <td>{{$item->producto_canje}}</td>
                                       <td>{{$item->monto_canje}}</td>
                                       <td>
                                   <a href="{{ url('/admin/configurationB/' . $item->id) }}" title="View Configuración"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/configurationB/' . $item->id . '/edit') }}" title="Edit Configuración"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/configurationB', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                               
                                            {!! Form::close() !!} 
                                        
                                       </td>
 
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
             
                        </div>

                    </div>
                </div>
    </div>
</div>
<script>
    // $('#fecha_inicio').mask("99/99/9999", {placeholder: 'MM/DD/YYYY' });
</script>
@endsection
 