@extends('layouts.backend')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h3 class="page-header">
            Configuración
        </h3>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active"> Configuración </li>
        </ol>

    </div>
    <div id="page-inner">

           <div class="row">
            <div class="col-md-12">
            <a href="/admin/configurationB" class="btn btn-sm btn-success">Edición de Configuración</a>
            </div>
        </div> 
          
             <div class="card" style="padding: 25px;">
                    <div class="card-header">Edit Configuración #{{ $configurations->id }} </div>
                    <div class="card-body">
                                       
                       
                      <a href="{{ url('/admin/configurationB') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás</button></a>
                        <br />
                        <br /> 

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($configurations, [
                            'method' => 'PATCH',
                            'url' => ['/admin/configurationB', $configurations->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!} 

                        @include ('configurationB.form', ['formMode' => 'edit'
                        ])

                        {!! Form::close() !!}

                    </div>

                    </div>
                </div>
    </div>
</div>
<script>
    // $('#fecha_inicio').mask("99/99/9999", {placeholder: 'MM/DD/YYYY' });
</script>
@endsection
 


{{-- @extends('layouts.backend')

@section('content')
    <div class="">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Edit Configuración #{{ $configurations->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/business') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($configurations, [
                            'method' => 'PATCH',
                            'url' => ['/admin/business', $configurations->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('configurationB.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection --}}
