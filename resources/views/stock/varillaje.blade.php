@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h3 class="page-header">
            STOCK|Varillajes
        </h3>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Stock</a></li>
            <li class="active">Varillajes</li>
        </ol>

    </div>
    <div id="page-inner">
        <div class="row">

            <div class="col-md-6">
                @foreach($tanks as $row)
                <div class="panel">
                    <div class="panel-body">
                        <form action="{{route('add-varillaje')}}" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-3 form-group">
                                    <label for="">Tanque</label>
                                    <input type="hidden" value="{{$row->id}}" name="id_tanque">
                                    <input type="text" name="" value="{{$row->name}}" class="form-control">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="">Fecha</label>
                                    <input type="date" name="fecha" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="">hora</label>
                                    <input type="time" name="hora" class="form-control" value="<?php echo date('H:i:s'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="">Varillaje</label>
                                    <input type="text" name="stock" class="form-control">
                                </div>
                                <div class="col-md-12 form-group">
                                    <button class="btn btn-primary">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Tanque</th>
                                        <th>Fecha</th>
                                        <th>Hora</th>
                                        <th>Varillaje</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($varillajes as $var)
                                    <tr>
                                        <td>{{ App\Http\Controllers\StockController::tankName($var->id_tanque) }}</td>
                                        <td>{{$var->fecha}}</td>
                                        <td>{{$var->hora}}</td>
                                        <td>{{$var->stock}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection