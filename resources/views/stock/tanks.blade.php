@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h3 class="page-header">
            STOCK|Tanques
        </h3>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Stock</a></li>
            <li class="active">Tanques</li>
        </ol>

    </div>
    <div id="page-inner">

        <div class="panel">
            <div class="panel-body p-2">
                <div class="row">
                    <div class="form">
                        @if(!$edit)
                        <form action="{{route('add-tank')}}" method="POST">
                            @else
                            <form action="{{route('update-tank',['id'=>$tank->id])}}" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                                @endif
                                @csrf
                                <div class="form-group col-md-3">
                                    <label for="">Nombre del tanque</label>
                                    <input required type="text" value="{{$tank->name}}" name="name" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Combustible</label>
                                    <select required class="form-control" name="id_producto" id="">
                                        @foreach($productos as $row)
                                        <option @if($row->id==$tank->id_producto) selected @endif value="{{$row->id}}">{{$row->descripcion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Stock</label>
                                    <input required type="text" value="{{$tank->stock}}" name="stock" class="form-control">
                                </div>
                                <div class="form-group col-md-12">
                                    <button class="btn btn-primary btn-sm">Grabar</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="panel">
            <div class="panel-body p-2">
                <div class="row">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Tanque</th>
                                <th>Combustible</th>
                                <th>Stock</th>
                                <th>Editar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tanks as $row)
                            <tr>
                                <td>{{$row->name}}</td>
                                <td>{{ App\Http\Controllers\StockController::productName($row->id_producto) }}</td>
                                <td>{{$row->stock}}</td>
                                <td><a href="{{route('edit-tank',['id'=>$row->id])}}" class="btn btn-sm">Editar</a> </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
</div>

@endsection