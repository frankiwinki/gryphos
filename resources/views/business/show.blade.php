@extends('layouts.backend')  
@section('content') 

<div id="page-wrapper">
  

    <div class="header">
        <h1 class="page-header">
            BUSINESS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Mantenimientos</a></li>
            <li class="active">BUSINESS</li>
        </ol>
 
    </div>
    <div id="page-inner">
    <div class="row">
            <div class="col-md-12">
            <a href="/pos/clientes" class="btn btn-sm btn-success">Listado de business</a>
            </div>
        </div>
      
        <div class="card" style="padding: 25px;">
        <div class="card-header">
            Business {{ $business->id }}
        </div>
        <div class="card-body">

             <div class="row">
                <div class="col-sm-12">
                    
                    <div class="col-sm-2">
                   

                    </div>

                    <div class="col-sm-4">

            <a href="{{ url('/admin/business') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás</button></a>
                        <a href="{{ url('/admin/business/' . $business->id . '/edit') }}" title="Edit Business"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/business', $business->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Business',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}

                    </div>
               </div>
                 
            </div>                                  
            <br/>
            <br/>

            <div class="table-responsive">

                <table class="table">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $business->id }}</td>
                        </tr>
                        <tr><th> Id </th><td> {{ $business->id }} </td></tr><tr><th> Business Name </th><td> {{ $business->business_name }} </td></tr><tr><th> Business Ruc </th><td> {{ $business->business_ruc }} </td></tr>
                    </tbody>
                </table>
                
            </div>
        </div>
        </div>

</div>

@endsection
