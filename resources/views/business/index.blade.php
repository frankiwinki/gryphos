@extends('layouts.backend')  
@section('content') 

<div id="page-wrapper">
  

    <div class="header">
        <h1 class="page-header">
            BUSINESS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Mantenimientos</a></li>
            <li class="active">BUSINESS</li>
        </ol>
 
    </div>
    <div id="page-inner">
    <div class="row">
            <div class="col-md-12">
            <a href="/pos/clientes" class="btn btn-sm btn-success">Listado de business</a>
            </div>
        </div>
      
        <div class="card" style="padding: 25px;">
        <div class="card-header">
            Business
        </div>
        <div class="card-body">

             <div class="row">
                <div class="col-sm-12">
                    
                    <div class="col-sm-2">
                        <a href="{{ url('/admin/business/create') }}" class="btn btn-success btn-sm" title="Add New Business">
                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar
                        </a>
                    </div>

                    <div class="col-sm-4">

                    {!! Form::open(['method' => 'GET', 'url' => '/admin/business', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                    
                        <input type="text" class="form-control col-sm-4" name="search" placeholder="Search..." value="{{ request('search') }}">
                        <span class="input-group-append col-sm-2">
                            <button class="btn btn-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>    
                    {!! Form::close() !!}

                    </div>
               </div>
                 
            </div>                

           
                        
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Id</th><th>Business Name</th><th>Business Ruc</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($business as $item)
                                    <tr>
                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td>{{ $item->id }}</td><td>{{ $item->business_name }}</td><td>{{ $item->business_ruc }}</td>
                                        <td>
                                            <a href="{{ url('/admin/business/' . $item->id) }}" title="View Business"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/business/' . $item->id . '/edit') }}" title="Edit Business"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/business', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Business',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $business->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>


        </div>
        </div>

   

</div>

@endsection

