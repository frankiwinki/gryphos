@extends('layouts.backend')  
@section('content') 

<div id="page-wrapper">
  

    <div class="header">
        <h1 class="page-header">
            BUSINESS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Mantenimientos</a></li>
            <li class="active">BUSINESS</li>
        </ol>
 
    </div>
    <div id="page-inner">
    <div class="row">
            <div class="col-md-12">
            <a href="/pos/clientes" class="btn btn-sm btn-success">Listado de business</a>
            </div>
        </div>
      
        <div class="card" style="padding: 25px;">
        <div class="card-header">
           Edit Business #{{ $business->id }}
        </div>
        <div class="card-body">

             <div class="row">
                <div class="col-sm-12">
                    
                    <div class="col-sm-2">
                   

                    </div>

                    <div class="col-sm-4">

                       <a href="{{ url('/admin/business') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás</button></a>
                                     @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($business, [
                            'method' => 'PATCH',
                            'url' => ['/admin/business', $business->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('business.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
               </div>   
            </div>                                  
        
        </div>
        </div>

</div>

@endsection






{{-- 

@extends('layouts.backend')

@section('content')
    <div class="">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Edit Business #{{ $business->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/business') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($business, [
                            'method' => 'PATCH',
                            'url' => ['/admin/business', $business->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('business.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection --}}
