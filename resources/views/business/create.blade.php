@extends('layouts.backend')

@section('content')

<div id="page-wrapper">
  

    <div class="header">
        <h1 class="page-header">
            BUSINESS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Mantenimientos</a></li>
            <li class="active">BUSINESS</li>
        </ol>
 
    </div>
    <div id="page-inner">
    <div class="row">
            <div class="col-md-12">
            <a href="/pos/clientes" class="btn btn-sm btn-success">Create New Business</a>
            </div>
        </div>
      
        <div class="card" style="padding: 25px;">
        <div class="card-header">
           
        </div>
        <div class="card-body">

             <div class="row">
                <div class="col-sm-12">
                    
                       <a href="{{ url('/admin/business') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/business', 'class' => '', 'files' => true]) !!}

                        @include ('business.form', ['formMode' => 'create'])

                        {!! Form::close() !!}
                   
                
               </div>
                 
            </div>                

           
                    

        </div>
        </div>

   

</div>

@endsection



