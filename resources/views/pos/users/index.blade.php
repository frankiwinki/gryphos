@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            USUARIOS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Usuarios</li>
        </ol>       
    </div>
    <div id="page-inner">
        <a href="/pos/users" class="btn btn-sm btn-success">Usuarios en el POS</a>
                    <a href="/pos/users/create"  class="btn btn-sm btn-primary text-white">Agregar Usuario
                    <i class="fa fa-plus"></i></a>
                    <br>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Usuario</th>
                                    <th>Nombres</th>
                                    <th>Estado</th>
                                    <th>Fecha de Creación</th>
                                    <th>Fecha de Actualización</th>
                                    <th>Editar</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($usuarios as $usuario)
                                <tr>
                                    <td>{{$usuario->id}}</td>
                                    <td>{{$usuario->usuario}}</td>
                                    <td>{{$usuario->nombres}}</td>
                                    <td>
                                        @switch($usuario->status)
                                        @case(0)
                                        <span> Deshabilitado</span>
                                        @break
                                        @case(1)
                                        <span>Habilitado</span>
                                        @break
                                        @endswitch
                                    </td>
                                    <td>{{$usuario->created_at}}</td>
                                    <td>{{$usuario->updated_at}}</td>
                                    <td><a href="/pos/users/{{$usuario->id}}/edit" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> </a></td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <hr>

                </div>
            </div>        
@endsection