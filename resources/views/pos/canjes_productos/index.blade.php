@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
           CONFIGURACIÓN CANJES - PRODUCTOS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Canjes - Productos</li>
        </ol>

    </div>  
    <div id="page-inner">
        <div class="row">
            <div class="col-md-6"> 
                <a href="/pos/canjesproductos" class="btn btn-sm btn-info">Listado de Canjes Productos</a>
                <a href="/pos/canjesproductos/create" class="btn btn-sm btn-primary text-white">Agregar
                    <i class="fa fa-plus"></i>
                </a>               
            </div>
            <div class="col-md-6">
                <form method="GET" action="/pos/canjesproductos" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" value="{{ old('search') }}" placeholder="Buscar...">
                        <span class="input-group-addon">
                            <button class="btn btn-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="card-body p-2">
            <br>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>PRODUCTO</th>
                            <th>MONTO CANJE</th>
                            <th>META CANJE</th>
                            <th>ESTADO</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                 <tbody>
                         
                        @foreach($canjesProductos as $item)
                        <tr>
                          <td>{{$item->id}}</td>
                           <td>{{ App\Http\Controllers\StockController::productName($item->id_producto) }}</td>
                            <td>{{$item->monto_canje}}</td>                        
                            <td>{{$item->meta_canje}}</td>
                            <td>                         
                              @if($item->estado =='A')         
                                    ACTIVO      
                                @else
                                    INACTIVO      
                                @endif</td>
                            <td>
                                <a class="btn btn-sm btn-primary" href="/canjesproductos/editar/{{$item->id}}">Editar</a>
                                <a target="_blank" href="/canjesproductos/detalle/{{$item->id}}" class="btn btn-info btn-sm">Ver</a>
                            </td>
                        </tr>
                        @endforeach 

 
                    </tbody>
                    
                </table>
                {{ $canjesProductos->links() }}
            </div>
        </div>
    </div>
</div>
@endsection