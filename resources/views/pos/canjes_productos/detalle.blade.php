
@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            CANJES - PRODUCTOS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Canjes - Productos </li>
        </ol>

    </div>
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <a href="/pos/canjesproductos" class="btn btn-sm btn-success">Visualización de Configuración CANJES - PRODUCTO</a>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <div id="agregar-usuario_pos">

                      <a href="{{ url('/pos/canjesproductos') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás</button></a>
                        <a href="{{ url('/canjesproductos/editar/' . $canjeProducto->id . '') }}" title="Edit Canjes - Producto"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE', 
                            'url' => ['admin/configurationB', $canjeProducto->id],
                            'style' => 'display:inline'
                        ]) !!} 
                           
                        {!! Form::close() !!}
                        <br/>
                        <br/> 

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $canjeProducto->id }}</td>
                                    </tr>
                                     <tr>
                                        <th>Producto</th><td>{{ App\Http\Controllers\StockController::productName($canjeProducto->id_producto) }}</td>
                                    </tr>
                                     <tr>
                                        <th>Monto Canje</th><td>{{ $canjeProducto->monto_canje }}</td>
                                    </tr>
                                     <tr>
                                        <th>Meta Canje</th><td>{{ $canjeProducto->meta_canje }}</td>
                                    </tr>
                                                              
                                </tbody>
                            </table>
                        </div>
                    
                
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
