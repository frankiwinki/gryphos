@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            CANJES - PRODUCTOS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Canjes - Productos </li>
        </ol>

    </div>
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <a href="/pos/canjes" class="btn btn-sm btn-success">Listado de Productos - Canjes</a>
                 <a href="{{ url('/pos/canjesproductos') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás</button></a>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <div id="agregar-usuario_pos">
                    
                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif
                    <form method="POST" action="/pos/canjesproductos/{{$canjesProductos->id}}">
                        {{ method_field('PUT') }}
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="nro_doc" class="col-form-label text-md-right">{{ __('ID') }}</label>
                                <input id="id" type="text" class="form-control" name="id" value="{{$canjesProductos->id}}" required disabled>
                                @error('nro_doc')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                         
                        </div>

                        <div class="form-group row">
                           
                            <div class="col-md-6">
                                <label for="">PRODUCTO</label>
                                <select name="id_producto" id="" class="form-control">
                                    @foreach($productos as $row)
                                    <option @if($row->id==$canjesProductos->id_producto) selected @endif value="{{$row->id}}">{{$row->descripcion}}</option>
                                    @endforeach
                                </select>                                
                            </div>

                             <div class="col-md-6">
                                <label for=""> MONTO CANJE</label>
                                <input type="text" name="monto_canje" value="{{$canjesProductos->monto_canje}}" class="form-control" autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="">META CANJE</label>
                                <input type="text" name="meta_canje" value="{{$canjesProductos->meta_canje}}" class="form-control">
                            </div>
                            
                        </div>
   <div class="form-group row">
                          <div class="col-md-6">
                                <label for="">ESTADO</label>
                                <select name="estado" id="" class="form-control">
                                     @if($canjesProductos->estado =='A')         
                                            <option  selected  value="A">Activo</option> 
                                             <option value="N">Inactivo</option> 
                                     @else
                                              <option value="A">Activo</option> 
                                              <option selected  value="N">Inactivo</option>  
                                    @endif

                                </select>                                
                            </div>
                                </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="col-md-12 btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection