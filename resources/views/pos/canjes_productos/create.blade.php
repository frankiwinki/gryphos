@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            CANJES - PRODUCTOS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active"> Canjes - Productos </li>
        </ol>

    </div>
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <a href="/pos/canjesproductos" class="btn btn-sm btn-success">Listado de CANJES - PRODUCTOS </a>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <div id="agregar-usuario_pos">
                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif
                    <form method="POST" action="/pos/canjesproductos">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="id" class="col-form-label text-md-right">{{ __('ID') }}</label>
                                <input id="id" type="text" class="form-control @error('id') is-invalid @enderror" name="id" value="{{ old('id') }}" required autocomplete="id" disabled>
                                @error('id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                     
                        </div>
                        <div class="form-group row">
                            
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="producto" class="col-form-label text-md-right">{{ __('PRODUCTO') }}</label>
                                <select name="id_producto" id="" class="form-control">
                                    @foreach($productos as $row)
                                    <option value="{{$row->id}}">{{$row->descripcion}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for=""> MONTO CANJE </label>
                                <input type="text" name="monto_canje" class="form-control" autofocus>
                            </div>
                        </div>

                       
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for=""> META CANJE </label>
                                <input type="text" name="meta_canje" class="form-control">
                            </div>
                           
                        </div>
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="col-md-12 btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection