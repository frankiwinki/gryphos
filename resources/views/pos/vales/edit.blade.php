@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            MOVIMIENTO-CREAR
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Movimientos</a></li>
            <li class="active">Crear</li>
        </ol>

    </div>


    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <a href="/pos/vales/listar/{{$creditos->id}}" class="btn btn-sm btn-success">Listado de Movimientos</a>
            </div>
        </div>
        <br>
        <?php if (5 > 4) : ?>
            <div id="agregar-vale">
                <h3>Editar vale</h3>
                <br>
                <form method="POST" action="/pos/vales">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-9">
                            <label for="password" class="col-form-label text-md-right">{{ __('Cliente') }}</label>
                            <select name="id_cliente" id_="id_cliente" onchange="buscarCreditos(this.value)" class="form-control js-select2">
                                <option value="{{$clientes->id}}">{{$clientes->nombres}}</option>

                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="password" class="col-form-label text-md-right">{{ __('Credito') }}</label>
                            <select required name="id_credito" id="id_credito" class="form-control js-select2">
                                <option value="{{$creditos->id}}">{{$creditos->id}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label for="monto" class="col-form-label text-md-right">{{ __('Número de Vale ') }}</label>
                            <input id="monto" type="text" class="form-control @error('monto') is-invalid @enderror" name="correlativo_inicio" value="{{ $vale->numero_vale}}" required autocomplete="monto" autofocus>
                        </div>

                        <div class="col-md-3">
                            <label for="monto" class="col-form-label text-md-right">{{ __('Monto') }}</label>
                            <input id="monto" type="text" class="form-control @error('monto') is-invalid @enderror" name="monto" value="{{ $vale->monto }}" required autocomplete="monto" autofocus>
                        </div>
                        <div class="col-md-3">
                            <label for="monto" class="col-form-label text-md-right">{{ __('Estado') }}</label>
                            <select name="" class="form-control" id="">
                                <option value=""></option>
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="form-group row mb-0">
                        <div class="col-md-12">
                            <button type="submit" class="col-md-12 btn btn-primary">
                                {{ __('Registrar') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <br>
        <?php endif; ?>

    </div>
</div>
<script>
    function buscarCreditos(id_cliente) {
        jQuery.get("../credito/" + id_cliente, function(data, status) {
            var creditos = document.getElementById("id_credito");
            LimpiarCombo("id_credito");
            for (var i = 0; i < data.length; i++) {
                var option = document.createElement("option");
                option.text = data[i].id;
                option.value = data[i].id;
                creditos.add(option);
            }
        });
    }

    function LimpiarCombo(id_combo) {
        var combo = document.getElementById(id_combo);
        var i;
        for (i = combo.options.length - 1; i >= 0; i--) {
            combo.remove(i);
        }
    }
    var factura = `<div class="form-group row mb-0">
   <div class="col-md-4">
      <label for="monto" class="col-form-label text-md-right">{{ __('Número de Factura') }}</label>
      <input  type="text" class="form-control" name="factura[]"  required  >
   </div>
   <div class="col-md-3">
      <label for="monto" class="col-form-label text-md-right">{{ __('Monto') }}</label>
      <input  type="text" class="form-control" name="monto_factura[]"  required  >
   </div>
</div>`;

    function agregarFactura() {
        $("#facturas").append(factura);
    }
</script>
@endsection