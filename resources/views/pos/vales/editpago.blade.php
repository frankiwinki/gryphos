@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
           PAGOS - EDITAR
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Pagos</a></li>
            <li class="active">Editar</li>
        </ol>

    </div>


    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <a href="/pos/vales/listar/{{$creditos->id}}" class="btn btn-sm btn-success">Listado de Movimientos</a>
                <a href="/pos/vales/listar/{{$creditos->id}}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás</button></a>
            </div>
        </div>
        <br>
 
        <div id="agregar-usuario_pos">
            <h3>Editar pago</h3>
            <br>
            <form method="POST" action="/pos/vales/editarPago">
                @csrf 
                <div class="form-group row">
                    <div class="col-md-9">
                        <label for="password" class="col-form-label text-md-right">{{ __('Cliente') }}</label>
                        <select name="id_cliente" id_="id_cliente" onchange="buscarCreditos(this.value)" class="form-control js-select2">
                            <option value="{{$clientes->id}}">{{$clientes->nombres}}</option>

                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="id_credito" class="col-form-label text-md-right">{{ __('Credito') }}</label>
                        <select required name="id_credito" id="id_credito" class="form-control js-select2">
                            <option value="{{$creditos->id}}">{{$creditos->id}}</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-4">
                        <label for="monto" class="col-form-label text-md-right">{{ __('Banco') }}</label>
                        <select name="banco" id_="banco" class="form-control js-select2">
                            @foreach($bancos as $row)
                            <option @if($row->id==$creditos->banco) selected @endif value="{{$row->id}}">{{$row->descripcion}}</option>
                            @endforeach
 
                        </select>
                    </div>
 
                    <div class="col-md-4">
                        <label for="monto" class="col-form-label text-md-right">{{ __('Tipo Pago') }}</label>
                        <select name="tipo_pago" id_="tipo_pago" class="form-control js-select2">
                            @foreach($tipopago as $row)        
                                  <option @if($row->id==$creditos->tipo_pago) selected @endif value="{{$row->id}}">{{$row->descripcion}}</option>
                            @endforeach

                        </select>
                    </div>
                   
                </div>

              <div class="form-group row mb-0">
                    <div class="col-md-3">
                        <label for="nro_operacion" class="col-form-label text-md-right">{{ __('Nro de Operación') }}</label>
                        <input id="nro_operacion"  value="{{$creditos->nro_operacion}}" type="text" class="form-control @error('nro_operacion') is-invalid @enderror" name="nro_operacion" required autocomplete="nro_operacion" autofocus>
                    </div>
                    <div class="col-md-3">
                        <label for="monto" class="col-form-label text-md-right">{{ __('Monto') }}</label>
                        <input id="monto"  value="{{$creditos->monto}}" type="text" class="form-control @error('monto') is-invalid @enderror" name="monto" required autocomplete="monto" autofocus>
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-12">
                        <a class="btn" onclick="agregarFactura()">Agregar Factura</a>
                    </div>
                </div>
                <div id="facturas">
                      <?php echo $detalle; ?>                   
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-12">
                        <button type="submit" class="col-md-12 btn btn-primary">
                            {{ __('Registrar') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function buscarCreditos(id_cliente) {
        jQuery.get("../credito/" + id_cliente, function(data, status) {
            var creditos = document.getElementById("id_credito");
            LimpiarCombo("id_credito");
            for (var i = 0; i < data.length; i++) {
                var option = document.createElement("option");
                option.text = data[i].id;
                option.value = data[i].id;
                creditos.add(option);
            }
        });
    }

    function LimpiarCombo(id_combo) {
        var combo = document.getElementById(id_combo);
        var i;
        for (i = combo.options.length - 1; i >= 0; i--) {
            combo.remove(i);
        }
    }

  function eliminarPagos(position) {
       
     
       $("#div_pagos_"+position+"").remove();

    }


 

    function agregarFactura() {

        var contadorActual= $('.contador_pagos').length;

   var factura = `<div class="form-group row mb-0 contador_pagos"  id="div_pagos_`+contadorActual+`">
   <div class="col-md-4">
      <label for="monto" class="col-form-label text-md-right">{{ __('Número de Factura') }}</label>
      <input  type="text" class="form-control" name="factura[]"  required  >
   </div>
   <div class="col-md-3">
      <label for="monto" class="col-form-label text-md-right">{{ __('Monto') }}</label>
      <input  type="text" class="form-control" name="monto_factura[]"  required  >
   </div>
   <div class="col-md-3" style="margin-top: 26px;"">                                        
      <a  class="btn btn-sm btn-danger"><i class="fa fa-times-circle" aria-hidden="true" onclick="eliminarPagos(`+contadorActual+`);"></i></a>
    </div>
</div>`;

        $("#facturas").append(factura);
    }
</script>
@endsection