@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            MOVIMIENTOS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Movimientos</li>
        </ol>

    </div>
    <div id="page-inner">
        @if(count($vales)>0)
        <h3>Cliente {{$vales[0]->nro_doc}} <br> {{$vales[0]->nombres}}</h3>
        @endif
        <br>
        <div class="row">
            <div class="col-md-4">
                <a href="/pos/vales/crear/{{$id_credito}}" class="btn btn-sm btn-primary text-white">Agregar Vale
                    <i class="fa fa-plus"></i>
                </a>
                <a href="/pos/vales/crear/{{$id_credito}}" class="btn btn-sm btn-primary text-white">Agregar Pago
                    <i class="fa fa-plus"></i>
                </a>
                <br>
                <a href="../exportar?id_producto={{$id_producto}}&id_credito={{$id_credito}}&fecha_ini={{$fecha_ini}}&&fecha_fin={{$fecha_fin}}" class="btn btn-success">
                    <i class="fa fa-file-excel-o"></i> EXPORTAR
                </a>
            </div>
            <div class="col-md-8">
                <form method="get" action="">
                    <div class="col-md-3">
                        <input type="date" class="form-control" value="{{$fecha_ini}}" name="fecha_ini" id="">
                    </div>
                    <div class="col-md-3">
                        <input type="date" class="form-control" value="{{$fecha_fin}}" name="fecha_fin" id="">
                    </div>
                    <div class="col-md-3">
                        <select name="id_producto" id="" class="form-control">
                            <option value="0">Todos</option>
                            @foreach($productos as $row)
                            <option value="{{$row->id}}">{{$row->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>


                    <button class="btn btn-success btn-sm">Buscar</button>
                </form>




            </div>
        </div>

        <div class="card-body p-2">

            <br>

            <br>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#consumos">Consumos</a></li>
                <li><a data-toggle="tab" href="#pagos">Pagos</a></li>
            </ul>
            <br><br>
            <div class="tab-content">
                <div id="consumos" class="tab-pane fade in active">
                    @if($vales!=null)
                    @if(count($vales)>0)

                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>N° de Vale</th>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Monto</th>
                                <th>Estado</th>
                                <th>Fecha</th>
                                <th>Detalles</th>
                                <th>Facturado</th>
                                <th>Editar</th>
                            </tr>
                            <thead>
                                @php $monto=0 @endphp
                                @php $cantidad=0 @endphp
                            <tbody>

                                @foreach($vales as $vale)
                                <tr class="vales_{{$vale->estado}}" id="fila_{{$vale->id}}">
                                    <td>{{$vale->id}}</td>
                                    <td>{{$vale->numero_vale}}</td>
                                    <td>{{$vale->producto}}</td>
                                    <td>{{$vale->cantidad}}</td>
                                    <td>{{$vale->monto}}</td>
                                    <td>@switch($vale->estado)
                                        @case('N')
                                        <span class="estado_despacho">Usado</span>
                                        @break
                                        @case('A')
                                        <span>Activo</span>
                                        @break
                                        @endswitch
                                    </td>
                                    <td>{{$vale->fecha_uso}}</td>
                                    <td><a onclick="verDetalle({{$vale->id}})" class="btn btn-sm btn-primary text-white" id="vale_detalle">ver</a></td>
                                    <td><a onclick="facturado({{$vale->id}})" class="btn btn-sm btn-primary text-white" id="vale_detalle">
                                            <i class="fa fa-check" aria-hidden="true"></i></a></td>
                                    <td><a href="/pos/vales/editar/{{$vale->id}}" class="btn btn-sm btn-primary text-white" id="vale_detalle">
                                    <i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                </tr>
                                @php $monto=$monto+$vale->monto @endphp
                                @php $cantidad=$cantidad+$vale->cantidad @endphp
                                @endforeach

                            </tbody>
                            <tr>
                                <td colspan="3"><b>TOTALES</b></td>
                                <td><b>{{$cantidad}}</b></td>
                                <td><b>{{$monto}}</b></td>
                                <td colspan="3"></td>
                            </tr>

                    </table>
                    {{ $vales->links() }}
                    @endif
                    @endif
                </div>

                <div id="pagos" class="tab-pane fade">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Banco</th>
                                <th>Nro Operación</th>
                                <th>Tipo</th>
                                <th>Monto</th>
                                <th>Estado</th>
                                <th>Fecha</th>
                                <th>Facturas</th>
                                <th>Acciones</th>
                            </tr>
                            <thead>
                                @php $monto=0 @endphp
                                @php $cantidad=0 @endphp
                            <tbody>
                                @foreach($pagos as $pago)

                                <tr> 
                                    <td>{{$pago->id}}</td>
                                    <td>{{ App\Http\Controllers\ValeController::bancoName($pago->banco) }}</td>
                                    <td>{{$pago->nro_operacion}}</td>
                                    <td>{{ App\Http\Controllers\ValeController::tipopagoName($pago->tipo_pago) }}</td> 
                                    <td>{{$pago->monto}}</td>
                                    <td>@switch($pago->estado)
                                        @case('N')
                                        <span>Usado</span>
                                        @break
                                        @case('A')
                                        <span>Activo</span>
                                        @break
                                        @endswitch
                                    </td>
                                    <td>{{$pago->created_at}}</td>
                                    <td>{{ App\Http\Controllers\ValeController::facturasPagoCreditos($pago->id) }}</td> 
                                    <td>
                                    <a onclick="verDetallePago({{$pago->id}})" class="btn btn-sm btn-primary text-white" id="vale_detalle">ver</a>                                
                                    <a href="/pos/vales/editarpago/{{$pago->id}}" class="btn btn-sm btn-primary text-white" id="vale_pago">
                                    <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    </td>
                                </tr> 

                                 @php $monto=$monto+$pago->monto @endphp

                                @endforeach
                            </tbody>
                            <tr>
                                <td colspan="4"><b>TOTALES</b></td>
                                <td><b>{{  number_format($monto, 2, '.', '') }}</b></td>
                                <td><b></b></td>
                                <td colspan="3"></td>
                            </tr>

                    </table>
                    {{ $vales->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal" tabindex="-1" id="vale_detalle_modal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detalles</h5>
                <button type="button" class="close" onclick="cerrarModal()" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="data_detalle"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary btn-close" onclick="cerrarModal()">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" id="vale_detalle_pago_modal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detalles de Pago</h5>
                <button type="button" class="close" onclick="cerrarModal()" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="data_detalle_pago"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary btn-close" onclick="cerrarModal()">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script>
    function verDetalle(id_vale) {
        $.get("../../vales/" + id_vale, function(data) {
            $("#data_detalle").html(data);
        });
        $("#vale_detalle_modal").fadeIn(400);
    } 


   function verDetallePago(id_pago) {
        $.get("../../vales/infoPagos/" + id_pago, function(data) {
            $("#data_detalle_pago").html(data);
        }); 
        $("#vale_detalle_pago_modal").fadeIn(400);
    } 
    

    function cerrarModal() {
        $("#vale_detalle_modal").fadeOut(400);
        $("#vale_detalle_pago_modal").fadeOut(400);

    }

    function facturado(id) {
        if (confirm("Está seguro que la venta fue facturada?")) {
            $("#fila_" + id).css("background", "yellow");
            $(".estado_despacho").text("Facturado");
        } else {
            $("#fila_" + id).css("background", "#f1f1f1");
            $(".estado_despacho").text("Pendiente");
        }
    }
</script>

@endsection