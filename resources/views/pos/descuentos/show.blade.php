@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            DESCUENTOS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Descuentos</li>
        </ol>

    </div>
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <a href="/pos/creditos" class="btn btn-sm btn-success">Listado de Clientes </a>
            </div>
        </div>

        <br>
        <div id="agregar-usuario_pos">
            <form method="POST" action="/pos/descuentos">
                @csrf
                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="password" class="col-form-label text-md-right">{{ __('Cliente') }}</label>
                        <select name="id_cliente" class="form-control js-select2">
                            @foreach ($clientes as $item)
                            <option value="{{$item->id}}">{{$item->nombres}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
    <br>
                <label for="">Productos</label>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>CÓDIGO</th>
                            <th>DESCRIPCIÓN</th>                            
                            <th>DESCUENTO</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $cont=0 @endphp
                        @foreach ($productos as $item) 
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->codigo}}</td>
                            <td>{{$item->descripcion}}</td>                            
                            <td>
                            <input type="hidden" value="{{$item->id}}" name="productos[{{$cont}}]">    
                            <input type="number" min="0" step="any" class="" required name="descuento[{{$cont}}]" value="{{$item->descuento}}"></td>
                        </tr>
                        @php $cont++ @endphp
                        @endforeach
                    </tbody>
                </table>
                <br>
                <div class="form-group row mb-0">
                    <div class="col-md-12">
                        <button type="submit" class="col-md-12 btn btn-primary">
                            {{ __('Registrar') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection