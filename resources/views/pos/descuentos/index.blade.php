@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            DESCUENTOS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Descuentos</li>
        </ol>

    </div>
    <div id="page-inner">
        <div class="row">
            <div class="col-md-6">
                <a href="/pos/descuentos" class="btn btn-sm btn-success">Listado de Descuentos</a>                
            </div>
            <div class="col-md-6">
                <form method="GET" action="/pos/descuentos" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" value="{{$search}}" placeholder="Search...">
                        <span class="input-group-addon">
                            <button class="btn btn-sm btn-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>

                    </div>
                    <br>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>N° Documento</th>
                            <th>Nombres</th>
                            <th>Descuentos</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($clientes as $row)
                        <tr>
                            <td>{{$row->nro_doc}}</td>
                            <td>{{$row->nombres}}</td>
                            <td><a class="btn btn-primary btn-sm" href="/pos/descuentos/ver/{{$row->id}}">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </a></td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection