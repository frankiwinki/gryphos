@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            CLIENTES
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Clientes</li>
        </ol>

    </div>
    <div id="page-inner">
        <div class="row">
            <div class="col-md-6">
                <a href="/pos/clientes" class="btn btn-sm btn-success">Listado de clientes</a>
                <a href="/pos/clientes/create" class="btn btn-sm btn-primary text-white">Agregar
                    <i class="fa fa-plus"></i>
                </a>
            </div>
            <div class="col-md-6">
                <form method="GET" action="/pos/clientes" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" value="{{ old('search') }}" placeholder="Buscar...">
                        <span class="input-group-addon">
                            <button class="btn btn-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="card-body p-2">
            <br>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>RUC</th>
                            <th>NOMBRES</th>
                            <th>ESTADO</th>
                            <th>CREDITOS</th>
                            <th>EDITAR</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($clientes as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->nro_doc}}</td>
                            <td>{{$item->nombres}}</td>
                            <td>
                                @switch($item->estado)
                                @case('N')
                                <span> Deshabilitado</span>
                                @break
                                @case('A')
                                <span>Habilitado</span>
                                @break
                                @endswitch
                            </td>
                            <td><a href="/pos/creditos?search={{$item->nro_doc}}" class="btn btn-primary btn-sm">ver</a>
                            </td>
                            <td><a href="/pos/clientes/{{$item->id}}/edit" class="btn btn-sm btn-dark">Editar</a> </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $clientes->links() }}
            </div>
        </div>
    </div>
</div>
@endsection