@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            CLIENTES
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Clientes</li>
        </ol>

    </div>
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
            <a href="/pos/clientes" class="btn btn-sm btn-success">Listado de clientes</a>
            </div>
        </div>
       
        <div id="agregar-usuario_pos">
            @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            @endif
            <form method="POST" action="/pos/clientes/{{$cliente->id}}">
                {{ method_field('PUT') }}
                @csrf
                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="nro_doc" class="col-form-label text-md-right">{{ __('RUC/DNI') }}</label>
                        <input id="nro_doc" type="text" class="form-control @error('nro_doc') is-invalid @enderror" name="nro_doc" value="{{ $cliente->nro_doc }}" required autocomplete="nro_doc" autofocus>
                        @error('nro_doc')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="nombres" class="col-form-label text-md-right">{{ __('Nombres/ Razon Social') }}</label>
                        <input id="nombres" type="text" class="form-control @error('nombres') is-invalid @enderror" name="nombres" value="{{ $cliente->nombres }}" required autocomplete="nombres" autofocus>
                        @error('nombres')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="distrito" class="col-form-label text-md-right">{{ __('Distrito') }}</label>
                        <input id="distrito" type="text" class="form-control @error('distrito') is-invalid @enderror" value="{{ $cliente->distrito }}" name="distrito" required autocomplete="distrito">

                        @error('distrito')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="provincia" class="col-form-label text-md-right">{{ __('Provincia') }}</label>
                        <input id="provincia" type="text" class="form-control @error('provincia') is-invalid @enderror" value="{{ $cliente->provincia }}" name="provincia" required autocomplete="provincia">

                        @error('provincia')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="departamento" class="col-form-label text-md-right">{{ __('Departamento') }}</label>
                        <input id="departamento" type="text" class="form-control @error('departamento') is-invalid @enderror" value="{{ $cliente->departamento }}" name="departamento" required autocomplete="provincia">

                        @error('departamento')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="estado" class="col-form-label text-md-right">{{ __('Estado') }}</label>
                        <select name="estado" class="form-control">
                            <option @if($cliente->estado=="A") selected @endif value="A" >Habilitado</option>
                            <option @if($cliente->estado=="N") selected @endif value="N" >Deshabilitado</option>
                        </select>
                        @error('estado')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-12">
                        <button type="submit" class="col-md-12 btn btn-primary">
                            {{ __('Registrar') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection