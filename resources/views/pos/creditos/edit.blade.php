@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            CRÉDITOS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Créditos</li>
        </ol>

    </div>
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <a href="/pos/creditos" class="btn btn-sm btn-success">Listado de Créditos</a>
            </div>
        </div>

        <br>
        <div id="agregar-usuario_pos">
            <form method="POST" action="/pos/creditos/{{$credito->id}}">
            {{ method_field('PUT') }}
                @csrf
                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="password" class="col-form-label text-md-right">{{ __('Cliente') }}</label>
                        <select name="id_cliente" class="class=" form-control js-select2">
                            @foreach ($clientes as $item)
                            <option value="{{$item->id}}">{{$item->nombres}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="monto" class="col-form-label text-md-right">{{ __('Monto') }}</label>
                        <input id="monto" value={{$credito->monto}} type="text" class="form-control @error('monto') is-invalid @enderror" name="monto" value="{{ old('monto') }}" required autocomplete="monto" autofocus>
                    </div>
                    <div class="col-md-6">
                        <label for="fecha" class="col-form-label text-md-right">{{ __('Fecha de Inicio') }}</label>
                        <input id="fecha" value="{{date('Y-m-d', strtotime($credito->created_at))}}" type="date" class="form-control @error('fecha') is-invalid @enderror" name="fecha" value="{{ old('fecha') }}" required>

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="monto" class="col-form-label text-md-right">{{ __('Consumido') }}</label>
                        <input  type="text" value="{{$credito->monto-$credito->saldo}}" class="form-control @error('consumido') is-invalid @enderror" name="consumido" value="{{ old('consumido') }}" required autocomplete="monto" autofocus>
                    </div>
                    <div class="col-md-6">
                        <label for="fecha" class="col-form-label text-md-right">{{ __('Saldo') }}</label>
                        <input  type="text"  value={{$credito->saldo}} class="form-control @error('saldo') is-invalid @enderror" name="saldo" value="{{ old('saldo') }}" required>

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <label for="">Productos</label>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>CÓDIGO</th>
                            <th>DESCRIPCIÓN</th>
                            <th>CANTIDAD</th>
                            <th>DESCUENTO</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $cont=0 @endphp
                        @foreach ($credito_productos as $item)                            
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->codigo}}</td>
                            <td>{{$item->descripcion}}</td>
                            <td>                                
                            <input type="hidden" name="credito_producto_id[{{$cont}}]" value="{{$item->id}}"   > 
                            <input type="hidden" name="codigo[{{$cont}}]" value="{{$item->id_producto}}"   > 
                            <input type="number" value="{{$item->cantidad}}" step="any" min="0" class="" required name="cantidad[{{$cont}}]" value="{{ old('cantidad.'.$item->id) }}"></td>
                            <td><input type="number" value="{{$item->descuento}}" min="0" step="any" class="" required name="descuento[{{$cont}}]" value="{{ old('cantidad.'.$item->id) }}"></td>
                        </tr>
                        @php $cont++ @endphp
                        @endforeach
                    </tbody>
                </table>
                <br>
                <div class="form-group row mb-0">
                    <div class="col-md-12">
                        <button type="submit" class="col-md-12 btn btn-primary">
                            {{ __('Registrar') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection