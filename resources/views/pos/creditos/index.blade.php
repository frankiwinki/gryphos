@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            CRÉDITOS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Créditos</li>
        </ol>

    </div>
    <div id="page-inner">
        <div class="row">
            <div class="col-md-6">
                <a href="/pos/creditos" class="btn btn-sm btn-success">Listado de Créditos</a>
                <a href="/pos/creditos/create" class="btn btn-sm btn-primary text-white">Agregar <i class="fa fa-plus"></i></a>
                <a href="/pos/creditos/exportar" class="btn btn-success btn-sm">
                    Exportar <i class="fa fa-file-excel-o"></i>
                </a>
            </div>
            <div class="col-md-6">
                <form method="GET" action="/pos/creditos" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" value="{{ old('search') }}" placeholder="Search...">
                        <span class="input-group-addon">
                            <button class="btn btn-sm btn-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>

                    </div>
                    <br>
                </form>
            </div>
        </div>
        <div class="">
            <br>
            <div class="table-responsive">
                <br>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID_CLIENTE</th>
                            <th>ID_CREDITO</th>
                            <th>FECHA</th>
                            <th>RUC</th>
                            <th>Cliente</th>
                            <th>Monto</th>
                            <th>Usado</th> 
                            <th>Saldo</th>
                            <th>Movimientos</th>
                            <th>Editar</th>
                            <th>Deshabilitar</th>
                        </tr>
                        <thead>
                        <tbody>
                            @foreach($creditos as $credito)
                            <tr>
                                <td>{{$credito->id_cliente}}</td>
                                <td>{{$credito->id}}</td>
                                <td>{{$credito->created_at}}</td>
                                <td>{{$credito->nro_doc}}</td>
                                <td>{{$credito->nombres}}</td>
                                <td>{{$credito->monto}}</td>
                                <td>{{$credito->monto-$credito->saldo}}</td>
                                <td>{{$credito->saldo}}</td>
                                <td class="text-center"><a class="btn btn-sm" target="_blank" href="vales/listar/{{$credito->id}}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                <td class="text-center">
                                    <a href="creditos/editar/{{$credito->id}}/" class="btn text-center btn-sm btn-info text-white "><i class="fa fa-edit "></i></a>
                                </td>
                                <td>
                                    <form action="{{ route('creditos.destroy', $credito->id) }}" method="POST">
                                        {{ csrf_field() }} {{ method_field('delete') }}
                                        <button type="submit" class="btn text-center btn-sm btn-danger text-white">
                                        <i class="fa fa-ban"></i>
                                        </button>
                                    </form>
                                </td>                                
                            </tr>
                            @endforeach
                        </tbody>
                </table>
                {{ $creditos->links() }}
            </div>
        </div>
    </div>
</div>
@endsection