@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            CLIENTES-CANJES
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Clientes-Canjes</li>
        </ol>

    </div>
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <a href="/pos/canjes" class="btn btn-sm btn-success">Listado de canjes</a>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <div id="agregar-usuario_pos">
                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif
                    <form method="POST" action="/pos/canjes/{{$canje->id}}">
                        {{ method_field('PUT') }}
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="nro_doc" class="col-form-label text-md-right">{{ __('RUC/DNI') }}</label>
                                <input id="nro_doc" type="text" class="form-control" name="id_cliente" value="{{$canje->id_cliente}}" required autofocus>
                                @error('nro_doc')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="nombres" class="col-form-label text-md-right">{{ __('Nombres/ Razon Social') }}</label>
                                <input id="nombres" type="text" class="form-control" name="nombres" value="{{$canje->nombres}}" autofocus>
                                @error('nombres')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="">CONSUMO TOTAL</label>
                                <input type="text" name="consumo" value="{{$canje->consumo}}" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label for="">PRODUCTO</label>
                                <select name="id_producto" id="" class="form-control">
                                    @foreach($productos as $row)
                                    <option @if($row->id==$canje->id_producto) selected @endif value="{{$row->id}}">{{$row->descripcion}}</option>
                                    @endforeach
                                </select>                                
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="">PUNTOS USADOS</label>
                                <input type="text" name="puntos_usados" value="{{$canje->puntos_usados}}" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label for="">PUNTOS ACTIVOS</label>
                                <input type="text" name="puntos_activos" value="{{$canje->puntos_activos}}" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="col-md-12 btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection