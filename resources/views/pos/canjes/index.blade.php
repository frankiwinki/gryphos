@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            CLIENTES-CANJES
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Clientes</li>
        </ol>

    </div>
    <div id="page-inner">
        <div class="row">
            <div class="col-md-6">
                <a href="/pos/canjes" class="btn btn-sm btn-info">Listado de clientes</a>
                <a href="/pos/canjes/create" class="btn btn-sm btn-primary text-white">Agregar
                    <i class="fa fa-plus"></i>
                </a>
                <a href="/canjes/exportar" class="btn btn-sm btn-success text-white">Exportar
                    <i class="fa fa-excel"></i>
                </a>
            </div>
            <div class="col-md-6">
                <form method="GET" action="/pos/canjes" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" value="{{ old('search') }}" placeholder="Buscar...">
                        <span class="input-group-addon">
                            <button class="btn btn-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="card-body p-2">
            <br>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>RUC/DNI</th>
                            <th>NOMBRES</th>
                            <th>PRODUCTO</th>
                            <th>CONSUMO TOTAL</th>
                            <th>PUNTOS USADOS</th>
                            <th>PUNTOS ACTIVOS</th>
                            <th>EDITAR</th>
                            <th>VENTAS</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($clientes as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->id_cliente}}</td>
                            <td>{{$item->nombres}}</td>
                            <td>{{ App\Http\Controllers\StockController::productName($item->id_producto) }}</td>
                            <td>{{$item->consumo}}</td>
                            <td>{{$item->puntos_usados}}</td>
                            <td>{{$item->puntos_activos}}</td>
                            <td><a class="btn btn-sm btn-primary" href="/pos/canjes/{{$item->id}}/edit">Editar</a></td>
                            <td><a target="_blank" href="/canjes/detalle/{{$item->id_cliente}}" class="btn btn-info btn-sm">Ver</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                    
                </table>
                {{ $clientes->links() }}
            </div>
        </div>
    </div>
</div>
@endsection