@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            CLIENTES-CANJES
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Clientes-Canjes</li>
        </ol>

    </div>
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <a href="/pos/canjes" class="btn btn-sm btn-success">Listado de canjes</a>
            </div>
        </div>
        <div class="" id="app">
            <div id="agregar-usuario_pos">
                @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
                @endif
                <form method="POST" action="/pos/canjes">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="nro_doc" class="col-form-label text-md-right">{{ __('RUC/DNI') }}</label>
                            <input id="nro_doc" v-model="num_doc" v-on:keypress.13="test()" type="text" class="form-control @error('nro_doc') is-invalid @enderror" name="id_cliente" value="{{ old('id_cliente') }}" required autocomplete="nro_doc" autofocus>
                            @error('nro_doc')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-md-8">
                            <label for="nombres" class="col-form-label text-md-right">{{ __('Nombres/ Razon Social') }}</label>
                            <input id="nombres" v-model="nombres" type="text" class="form-control @error('nombres') is-invalid @enderror" name="nombres" value="{{ old('nombres') }}" required autocomplete="nombres" autofocus>
                            @error('nombres')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="">CONSUMO TOTAL</label>
                            <input type="text" v-model="consumo" name="consumo" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="">PUNTOS USADOS</label>
                            <input type="text" v-model="puntos_usados" name="puntos_usados" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="">PUNTOS ACTIVOS</label>
                            <input type="text" v-model="puntos_activos" name="puntos_activos" class="form-control">
                        </div>
                    </div>
                </form>
                <div class="form-group row">
                    <div class="col-md-12">
                        <button v-on:click="agregarDetalle()" class="btn btn-info">Agregar Detalle</button>
                        <br>
                    </div>
                    <br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="">COMBO</th>
                                <th>CANTIDAD</th>
                                <th class="text-center">ELIMINAR</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="item in detalles">
                                <td>
                                    <select v-model="item.producto" name="" id="" class="form-control">
                                        <option v-for="option in productos" v-bind:value="option.id">@{{ option.descripcion }}</option>
                                    </select>
                                </td>
                                <td><input size="2" v-model=item.cantidad class="form-control" type="text"></td>
                                <td class="text-center">
                                    <a v-on:click="eliminarDetalle(item)"  class="btn btn-danger btn-sm">Eliminar</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-12">
                        <button type="submit" class="col-md-12 btn btn-primary">
                            {{ __('Registrar') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js"></script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            puntos_activos: null,
            consumo: null,
            puntos_usados: null,
            nombres: null,
            num_doc: null,
            detalles: [],
            productos: null
        },
        mounted(){
            this.listarProductos();
        },
        methods: {
            test: function() {
                axios.get('/canjes/cliente/' + this.num_doc).then(
                    response => (
                        this.nombres = response.data.nombres,
                        this.puntos_activos = response.data.puntos_activos,
                        this.puntos_usados = response.data.puntos_usados,
                        this.consumo = response.data.consumo
                    )
                )
            },
            listarProductos: async function() {
                await axios.get('/productos').then(
                    response => (
                        this.productos = response.data
                    )
                )
                return this.productos;
            },
            agregarDetalle: async function() {                
                this.detalles.push({                    
                    cantidad: 0
                });
                // console.log(this.detalles);
            },
            eliminarDetalle:function(item){
                let idx=this.detalles.indexOf(item);
                console.log(idx);
                this.detalles.splice(idx,1);
                
            }
        }
    })
</script>
@endsection