@extends('layouts.dashboard')
@section('content')
<div class="" id="app">
    <div id="page-wrapper">
        <div class="header">
            <h1 class="page-header">
                CLIENTES-CANJES
            </h1>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Dashboard</a></li>
                <li class="active">Clientes</li>
            </ol>
        </div>
        <div id="page-inner">
            <div class="row">
                <div class="col-md-6">
                    <a href="/pos/canjes" class="btn btn-sm btn-info">Listado de clientes</a>
                    <a href="/pos/canjes/create" class="btn btn-sm btn-primary text-white">Agregar
                        <i class="fa fa-plus"></i>
                    </a>
                    <a href="{{route('exportar-canjes-detalle',['id'=>$id])}}" class="btn btn-success">Exportar</a>
                </div>
                <div class="col-md-6">
                    <form method="GET" action="/pos/canjes" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" value="{{ old('search') }}" placeholder="Buscar...">
                            <span class="input-group-addon">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal" id="modal-detalle" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Detalles</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Producto</th>
                                        <th>Cantidad</th>
                                        <th>Precio</th>
                                        <th>Monto</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="item in detalles" :key="item.id">
                                        <td>@{{item.descripcion}}</td>
                                        <td>@{{item.cantidad}}</td>
                                        <td>@{{item.ppu}}</td>
                                        <td>@{{item.monto}}</td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <div class="card-body p-2">
                <br>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>FECHA</th>
                                <th>NUM.DOC</th>
                                <th>NOMBRES</th>
                                <th>SERIE</th>
                                <th>CORRELATIVO</th>
                                <th>PRODUCTO</th>
                                <th>TOTAL</th>
                                <th>DETALLES</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ventas as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{date('d-m-Y', strtotime($item->fecha))}}</td>
                                <td>{{$item->ruc}}</td>
                                <td>{{$item->nombre}}</td>
                                <td>{{$item->numero_de_serie}}</td>
                                <td>{{$item->correlativo}}</td>
                                <td>{{$item->descripcion}}</td>
                                <td>{{$item->importe_total}}</td>
                                <td><a @click="listarDetalle({{$item->id}})" class="btn btn-info btn-sm">Ver</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </div>
</div>
</div>
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js"></script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            puntos_activos: null,
            consumo: null,
            puntos_usados: null,
            nombres: null,
            num_doc: null,
            detalles: [],
            productos: null,
            modalshow: false,
        },
        mounted() {},
        methods: {
            listarDetalle: async function(id) {
                await axios.get('/ventas/detalle/' + id).then(
                    response => (
                        this.detalles = response.data
                    )
                )
                $('#modal-detalle').modal('show');
                console.log(this.detalles);
                return this.detalles;
            },
        }
    })
</script>
@endsection