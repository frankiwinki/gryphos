@extends('layouts.backend')

@section('content')
<div class="">
    <div class="row">
        @include('admin.sidebar')
        <div class="col-md-9">
            @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            @endif

            <div class="card">
                <div class="card-header">
                    <h5>Cajas de Punto de Venta</h5>
                </div>
                <div class="card-body p-2">
                    <form method="GET" action="/pos/caja" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="form-group">
                            <label for="">Fecha</label>
                            <input type="date" class="form-control" name="fecha" value="{{$fecha}}">
                        </div>
                        <div class="form-group">
                            <label for="">Turno</label>
                            <select name="turno" value="{{ old('turno') }}" id="" class="form-control">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button class=" col-md-12  btn btn-sm btn-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                </div>
                </form>
                <br>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>TURNO</th>
                                <th>FECHA</th>
                                <th>COMBUSTIBLES</th>
                                <th>BOVEDAS</th>
                                <th>TARJETAS</th>
                                <th>CRÉDITOS</th>
                                <th>DESCUENTOS</th>
                                <th>OTROS</th>
                                <th>DEV. TANQUE</th>
                                <th>EGRESOS</th>
                                <th>ADELANTOS</th>
                                <th>CANJES</th>
                                <th>EFECTIVO</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($cajas!=null)
                            @foreach($cajas as $caja)
                            <tr>
                                <td>{{$caja->id_pos}}</td>
                                <td>{{$caja->turno}}</td>
                                <td>{{$caja->fecha}}</td>
                                <td>{{$caja->total_combustibles}}</td>
                                <td>{{$caja->total_depositos}}</td>
                                <td>{{$caja->total_tarjetas}}</td>
                                <td>{{$caja->total_creditos}}</td>
                                <td>{{$caja->descuentos}}</td>
                                <td>{{$caja->total_otros}}</td>
                                <td>{{$caja->dev_tanque}}</td>
                                <td>{{$caja->egresos}}</td>
                                <td>{{$caja->adelantos}}</td>
                                <td>{{$caja->canjes}}</td>
                                <td>{{$caja->total_combustibles-
                                        ($caja->total_depositos+$caja->total_creditos+$caja->descuentos+$caja->canjes)
                                        +$caja->total_otros                                        
                                    }}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <a href="#" onclick="verResumen()" class="mb-2 btn btn-sm btn-primary">Ver Resumen de Caja</a>
                    <br>
                </div>
                <div class="card" id="resumen-caja">
                    <div class="card-header">
                        <h5>Resumen de Caja</h5>
                    </div>
                    @if($caja_resumen!=null)
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-stripped">
                                    <thead>
                                        <tr>
                                            <th class="w-50">INGRESO BRUTO</th>
                                            <th class="w-25"><input type="text" class="form-control text-right" value="{{$caja_resumen->total_combustibles}}"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>CRÉDITOS</td>
                                            <td><input type="text" class="form-control text-right" value="{{$caja_resumen->total_creditos}}"> </td>
                                        </tr>
                                        <tr>
                                            <td>TARJETAS</td>
                                            <td><input type="text" class="form-control text-right" value="{{$caja_resumen->total_tarjetas}}"> </td>
                                        </tr>
                                        <tr>
                                            <td>DESCUENTOS</td>
                                            <td><input type="text" class="form-control text-right" value="{{$caja_resumen->descuentos}}"> </td>
                                        </tr>
                                        <tr>
                                            <td>DEVOLUCIÓN</td>
                                            <td><input type="text" class="form-control text-right" value="{{$caja_resumen->dev_tanque}}"> </td>
                                        </tr>
                                        <tr>
                                            <td>EGRESOS</td>
                                            <td><input type="text" class="form-control text-right" value="{{$caja_resumen->egresos}}"> </td>
                                        </tr>
                                        <tr>
                                            <td>ADELANTOS</td>
                                            <td><input type="text" class="form-control text-right" value="{{$caja_resumen->adelantos}}"> </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>EFECTIVO</th>
                                            <th><input type="text" class="form-control text-right" value="{{$caja_resumen->efectivo}}"> </th>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td><button class="col-md-12 btn btn-primary btn-sm text-center">Guardar</button></td>
                                        </tr>
                                    </tfoot>
                                    </tbody>
                                </table>

                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" id="caja-detalle" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="">TURNO</label>
                            <input type="hidden" id="id_caja">
                            <input type="text" id="turno" class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">COMBUSTIBLE</label>
                            <input type="text" id="total_combustible" class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">BOVEDAS</label>
                            <input type="text" id="total_deposito" class="form-control">
                        </div>
                    </div>
                    <div class="row">

                        <div class="form-group col-md-4">
                            <label for="">TARJETAS</label>
                            <input type="text" id="total_tarjeta" class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">CRÉDITOS</label>
                            <input type="text" id="total_credito" class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">DESCUENTOS</label>
                            <input type="text" id="total_descuento" class="form-control">
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="">OTROS</label>
                            <input type="text" id="total_otro" class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">DEV. TANQUE</label>
                            <input type="text" id="dev_tanque" class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">EGRESOS</label>
                            <input type="text" id="egresos" class="form-control">
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="">CANJES</label>
                            <input type="text" id="canjes" class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">EFECTIVO</label>
                            <input type="text" id="efectivo" class="form-control">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="editarDetalles()">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    function verDetalle(id_caja) {
        $.get("caja/" + id_caja, function(data) {
            var obj = data;
            document.getElementById("id_caja").value = id_caja;
            var turno = document.getElementById("turno").value = obj.turno;
            var total_combustible = parseFloat(document.getElementById("total_combustible").value = obj.total_combustibles);
            var total_deposito = parseFloat(document.getElementById("total_deposito").value = obj.total_depositos);
            var total_credito = parseFloat(document.getElementById("total_credito").value = obj.total_creditos);
            var total_otro = parseFloat(document.getElementById("total_otro").value = obj.total_otros);
            var descuentos = parseFloat(document.getElementById("total_descuento").value = obj.descuentos);
            var efectivo = total_combustible - (total_deposito + total_credito) + total_otro;
            var canjes = parseFloat(document.getElementById("canjes").value = obj.canjes);
            var egresos = parseFloat(document.getElementById("egresos").value = obj.egresos);
            var dev_tanque = parseFloat(document.getElementById("dev_tanque").value = obj.dev_tanque);
            var total_tarjeta = parseFloat(document.getElementById("total_tarjeta").value = obj.total_tarjetas);
            document.getElementById("efectivo").value = efectivo;
        });
        $("#caja-detalle").modal("show");
    }

    function editarDetalles() {
        var id_caja = document.getElementById("id_caja").value;
        var obj = [];
        var turno = document.getElementById("turno").value;
        var total_combustible = document.getElementById("total_combustible").value;
        var total_deposito = document.getElementById("total_deposito").value;
        var total_credito = document.getElementById("total_credito").value;
        var total_otro = document.getElementById("total_otro").value;
        var efectivo = document.getElementById("efectivo").value;
        var descuentos = document.getElementById("total_descuento").value;
        var canjes = document.getElementById("canjes").value;
        var egresos = document.getElementById("egresos").value;
        var dev_tanque = document.getElementById("dev_tanque").value;
        var total_tarjeta = document.getElementById("total_tarjeta").value;
        $.ajax({
            url: 'caja/' + id_caja,
            type: 'PUT',
            data: {
                id: id_caja,
                total_combustible: total_combustible,
                total_deposito: total_deposito,
                total_credito: total_credito,
                total_otro: total_otro,
                canjes: canjes,
                egresos: egresos,
                descuentos: descuentos,
                dev_tanque: dev_tanque,
                total_tarjeta: total_tarjeta,
                _token: '{{csrf_token()}}'
            },
            success: function(data) {
                window.location.reload();
                // console.log(data);
            }
        });
    }

    function verResumen() {
        var element = document.getElementById('resumen-caja');
        element.style.display = "block";
    }
</script>
@endsection