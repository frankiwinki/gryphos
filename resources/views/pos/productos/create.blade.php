@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            PRODUCTOS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Productos</li>
        </ol>
    </div>
    <div id="page-inner">
        <a href="/pos/productos" class="btn btn-sm btn-success">Listado de Productos</a>
        <div class="">
            <br>
            <div id="agregar-usuario_pos">
                <form method="POST" action="/pos/productos">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-1">
                            <label for="password" class="col-form-label text-md-right">{{ __('Codigo') }}</label>
                            <input type="text" class="form-control" name="codigo">
                        </div>
                        <div class="col-md-5">
                            <label for="password" class="col-form-label text-md-right">{{ __('Nombre del Producto') }}</label>
                            <input type="text" class="form-control" name="descripcion">
                        </div>
                        <div class="col-md-4">
                            <label for="password" class="col-form-label text-md-right">{{ __('Tipo de Combustible') }}</label>
                            <select name="tipo" class="form-control ">
                                <option value="P">Combustible</option>
                                <option value="X">Otro</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label for="password" class="col-form-label text-md-right">{{ __('Precio') }}</label>
                            <input type="number" name="precio" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="monto" class="col-form-label text-md-right">{{ __('Código de Barra') }}</label>
                            <input id="cod_barra" type="text" class="form-control @error('monto') is-invalid @enderror" name="monto" value="{{ old('monto') }}" required autocomplete="monto" autofocus>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12">
                            <button type="submit" class="col-md-12 btn btn-primary">
                                {{ __('Registrar') }}
                            </button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <br>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>CODIGO</th>
                                <th>NOMBRES</th>
                                <th>TIPO</th>
                                <th>COD. BARRA</th>
                                <th>PRECIO</th>
                            </tr>
                        </thead>
                        <tbody>@if($productos!=null)
                            @foreach($productos as $row)
                            <tr>
                                <td>{{$row->id}}</td>
                                <td>{{$row->codigo}}</td>
                                <td>{{$row->descripcion}}</td>
                                <td>
                                    @switch($row->tipo)
                                    @case('P')
                                    <span> Combustible</span>
                                    @break
                                    @case('X')
                                    <span>Otro</span>
                                    @break
                                    @endswitch
                                </td>
                                <td>{{$row->cod_barra}}</td>
                                <td>{{$row->precio}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    {{ $productos->links() }}
                </div>
            </div>

        </div>

    </div>
</div>
@endsection