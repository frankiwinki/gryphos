@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            PRODUCTOS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Productos</li>
        </ol>

    </div>
    <div id="page-inner">        
    <a href="/pos/productos" class="btn btn-sm btn-success">Listado de Productos</a>
            <div class="">
                    <br>
                    <div class=" col-md-12">
                        <form method="POST" action="/pos/productos/{{$producto->id}}">                        
                        {{ method_field('PUT') }}
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-1">
                                    <label for="password" class="col-form-label text-md-right">{{ __('Codigo') }}</label>
                                    <input type="text" class="form-control" name="codigo" value="{{$producto->codigo}}">
                                </div>
                                <div class="col-md-5">
                                    <label for="password" class="col-form-label text-md-right">{{ __('Nombre del Producto') }}</label>
                                    <input type="text" class="form-control" name="descripcion" value="{{$producto->descripcion}}">
                                </div>
                                <div class="col-md-4">
                                    <label for="password" class="col-form-label text-md-right">{{ __('Tipo de Combustible') }}</label>
                                    <select name="tipo" class="form-control ">
                                        <option value="P" @if($producto->tipo=="P") selected  @endif>Combustible</option>
                                        <option value="X" @if($producto->tipo=="X") selected  @endif>Otro</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="password" class="col-form-label text-md-right">{{ __('Precio') }}</label>
                                    <input type="number" step="any" name="precio" class="form-control" value="{{$producto->precio}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="monto" class="col-form-label text-md-right">{{ __('Código de Barra') }}</label>
                                    <input id="cod_barra" type="text" class="form-control @error('cod_barra') is-invalid @enderror" name="cod_barra" value="{{ $producto->cod_barra}}"  >
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12">
                                    <button type="submit" class="col-md-12 btn btn-primary">
                                        {{ __('Registrar') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <br>                    
                </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
@endsection