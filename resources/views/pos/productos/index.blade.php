@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            PRODUCTOS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Productos</li>
        </ol>

    </div>
    <div id="page-inner">
        <div class="row">
            <div class="col-md-6">
                <a href="/pos/productos" class="btn btn-sm btn-success">Listado de Productos</a>
                <a href="/pos/productos/create" class="btn btn-sm btn-primary text-white">Agregar
                    <i class="fa fa-plus"></i>
                </a>
                <a href="/pos/productos/exportar" class="btn btn-success btn-sm">
                    Exportar <i class="fa fa-file-excel-o"></i>
                </a>
            </div>
            <div class="col-md-6">
            <form method="GET" action="/pos/productos" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                <div class="input-group">
                    <input type="text" class="form-control text-uppercase" name="search" value="{{ old('search') }}" placeholder="Search...">
                    <span class="input-group-addon">
                        <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>
            </div>
        </div>

        <div class="card-body p-2">           
            <br>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>CODIGO</th>
                            <th>NOMBRES</th>
                            <th>TIPO</th>
                            <th>COD. BARRA</th>
                            <th>PRECIO</th>
                            <th>EDITAR</th>
                        </tr>
                    </thead>
                    <tbody>@if($productos!=null)
                        @foreach($productos as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->codigo}}</td>
                            <td>{{$row->descripcion}}</td>
                            <td>
                                @switch($row->tipo)
                                @case('P')
                                <span> Combustible</span>
                                @break
                                @case('X')
                                <span>Otro</span>
                                @break
                                @endswitch
                            </td>
                            <td>{{$row->cod_barra}}</td>
                            <td>{{$row->precio}}</td>
                            <td><a class="btn btn-sm" href="productos/{{$row->id}}/edit/"><i class="fa fa-edit "></i></a></td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
@endsection