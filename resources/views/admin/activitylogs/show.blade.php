@extends('layouts.backend')  
@section('content') 

<div id="page-wrapper">
  

    <div class="header">
        <h1 class="page-header">
            Activity Logs
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Mantenimientos</a></li>
            <li class="active">Activity Logs</li>
        </ol>
 
    </div>
    <div id="page-inner">
    <div class="row">
            <div class="col-md-12">
            <a href="/pos/clientes" class="btn btn-sm btn-success">Listado de Activity Logs</a>
            </div>
        </div>
      
        <div class="card" style="padding: 25px;">
        <div class="card-header">
            Activity {{ $activitylog->id }}
        </div>
        <div class="card-body">

             <div class="row">
                <div class="col-sm-12">
                    
                    <div class="col-sm-2">
                   

                    </div>

                    <div class="col-sm-4">

             <a href="{{ url('/admin/activitylogs') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/activitylogs', $activitylog->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Activity',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}

                    </div>
               </div>
                 
            </div>                                  
            <br/>
            <br/>

            <div class="table-responsive">

             <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $activitylog->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Activity </th><td> {{ $activitylog->description }} </td>
                                    </tr>
                                    <tr>
                                        <th> Actor </th>
                                        <td>
                                            @if ($activitylog->causer)
                                                <a href="{{ url('/admin/users/' . $activitylog->causer->id) }}">{{ $activitylog->causer->name }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Date </th><td> {{ $activitylog->created_at }} </td>
                                    </tr>
                                </tbody>
                            </table>
                
            </div>
        </div>
        </div>

</div>

@endsection

