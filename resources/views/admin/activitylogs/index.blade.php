@extends('layouts.backend')
@section('content') 

<div id="page-wrapper">
  

    <div class="header">
        <h1 class="page-header">
            Activity Logs
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Mantenimientos</a></li>
            <li class="active">Activity Logs</li>
        </ol>
 
    </div>
    <div id="page-inner">
    <div class="row">
            <div class="col-md-12">
            <a href="/pos/clientes" class="btn btn-sm btn-success">Listado de Activity Logs</a>
            </div>
        </div>
      
        <div class="card" style="padding: 25px;">
        <div class="card-header">
            Activity Logs
        </div>
        <div class="card-body">

             <div class="row">
                <div class="col-sm-12">
                    
                    <div class="col-sm-6">

                      {!! Form::open(['method' => 'GET', 'url' => '/admin/activitylogs', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                       
                            <input type="text" class="form-control col-sm-4" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append col-sm-2">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                      
                        {!! Form::close() !!}

                    </div>
               </div>
                 
            </div>                

           
                        
                        <br/>
                        <br/>
                       <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th><th>Activity</th><th>Actor</th><th>Date</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($activitylogs as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>
                                            @if ($item->causer)
                                                <a href="{{ url('/admin/users/' . $item->causer->id) }}">{{ $item->causer->name }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>{{ $item->created_at }}</td>
                                        <td>
                                            <a href="{{ url('/admin/activitylogs/' . $item->id) }}" title="View Activity"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/activitylogs', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Activity',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $activitylogs->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>


        </div>
        </div>

   

</div>

@endsection


