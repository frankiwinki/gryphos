<div class="form-group{{ $errors->has('business_name') ? 'has-error' : ''}}">
    {!! Form::label('business_name', 'Business Name', ['class' => 'control-label']) !!}
    {!! Form::text('business_name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('business_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('business_ruc') ? 'has-error' : ''}}">
    {!! Form::label('business_ruc', 'Business Ruc', ['class' => 'control-label']) !!}
    {!! Form::text('business_ruc', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('business_ruc', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('ip') ? 'has-error' : ''}}">
    {!! Form::label('ip', 'Ip', ['class' => 'control-label']) !!}
    {!! Form::text('ip', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('ip', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('connection_string') ? 'has-error' : ''}}">
    {!! Form::label('connection_string', 'Connection String', ['class' => 'control-label']) !!}
    {!! Form::text('connection_string', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('connection_string', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
    {!! Form::textarea('status', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
 