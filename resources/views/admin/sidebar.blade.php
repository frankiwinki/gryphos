 <ul class="nav" id="main-menu">
    @foreach($laravelAdminMenus->menus as $section)
    @if($section->items)
    @if($section->section=="Administrador")
    @if(Auth::user()->hasRole('admin'))
    <li>
            
        <a href="#" class="waves-effect waves-dark">
                            <i class="fa fa-table"></i> {{ $section->section }}<span class="fa arrow"></span>
                        </a>

         <ul class="nav nav-second-level">
                @foreach($section->items as $menu)
                <li>
                    <a class="waves-effect waves-dark" href="{{ url($menu->url) }}">
                        {{ $menu->title }}
                    </a>
                </li>
                @endforeach
            </ul>
       
    </li>
    <br/>
    @endif
    @else
    <li>
         <a href="#" class="waves-effect waves-dark">
                            <i class="fa fa-table"></i> {{ $section->section }}<span class="fa arrow"></span>
                        </a>
            <ul class="nav nav-second-level">
                @foreach($section->items as $menu)
                <li class="nav-item" role="presentation">
                    <a class="waves-effect waves-dark" href="{{ url($menu->url) }}">
                        {{ $menu->title }}
                    </a>
                </li>
                @endforeach
            </ul>
    </li>
    <br />
    @endif

    @endif
    @endforeach
</ul>