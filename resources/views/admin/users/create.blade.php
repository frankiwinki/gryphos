@extends('layouts.backend')

@section('content')
  <div id="page-wrapper">
  

    <div class="header">
        <h3 class="page-header">
            USUARIOS
        </h3>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Mantenimientos</a></li>
            <li class="active">Usuarios</li>
        </ol>
 
    </div>
    <div id="page-inner">

            
                <div class="panel">
                    <div class="panel-header">Crear nuevo usuario</div>
                    <div class="panel-body p-2">
                        <a href="{{ url('/admin/users') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/users', 'class' => 'form-horizontal']) !!}

                        @include ('admin.users.form', ['formMode' => 'Registrar'])

                        {!! Form::close() !!}

                    </div>
                </div>
           
        </div>
    </div>
        </div>
@endsection
