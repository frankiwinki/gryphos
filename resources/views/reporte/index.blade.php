@extends('layouts.backend')

@section('content')
    <div class="">
    <div class="row">
        @include('admin.sidebar')
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Reportes</div>
                <div class="card-body p-2">
                    <h4>Reporte de Ventas del Mes</h4>
                    <table class="table table-striped">
                        <thead>
                            <tr >
                                <th>ID</th>
                                <th>ID_CLIENTE</th>
                                <th>Fecha</th>
                                <th>Monto</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ventas as $venta)
                            <tr>
                                <td>{{$venta->id}}</td>
                                <td>{{$venta->id_cliente}}</td>
                                <td>{{$venta->fecha}}</td>
                                <td>{{$venta->total}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection