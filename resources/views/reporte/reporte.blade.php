@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            REPORTE DE VENTAS
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Reporte de Ventas</li>
        </ol>

    </div>
    <div id="page-inner">
        <form method="GET" action="/admin/reportes/buscar" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
            <div class="">
                <div class="form-group">
                    <div class="">
                        <label for="">Punto de Venta</label>
                    </div>
                    <div class="">
                        <select style="width: 100%;" name="id_pos" id="" class="form-control">
                            <option @if($id_pos==0) selected @endif value="0">Todos</option>
                            <option @if($id_pos==1) selected @endif value="1">1</option>
                            <option @if($id_pos==2) selected @endif value="2">2</option>
                            <option @if($id_pos==3) selected @endif value="3">3</option>
                            <option @if($id_pos==4) selected @endif value="4">4</option>
                            <option @if($id_pos==5) selected @endif value="5">5</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="">
                        <label for="">Documento</label>
                    </div>
                    <div class="">
                        <select name="tipo_doc" id="" class="form-control">
                            <option @if($tipo_doc=="0" ) selected @endif value="0">Todos...</option>
                            <option @if($tipo_doc=="F" ) selected @endif value="F">Facturas</option>
                            <option @if($tipo_doc=="B" ) selected @endif value="B">Boletas</option>
                        </select>
                    </div>

                </div>
                <div class="form-group">
                    <div class="">
                        <label for="">Fecha de Inicio </label>
                    </div>
                    <div class="">
                        <input type="date" class="form-control" name="fecha_inicio" value="{{ $fecha_inicio }}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="">
                        <label for="">Fecha de Fin </label>
                    </div>
                    <div class="">
                        <input type="date" class="form-control" name="fecha_fin" value="{{ $fecha_fin }}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="">
                        <label style="color: transparent">Botones</label>
                    </div>
                    <span class="input-group">
                        <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                    <span class="ml-2 input-group">
                        <a href="exportar?fecha_inicio={{$fecha_inicio}}&&fecha_fin={{$fecha_fin}}&&tipo_doc={{$tipo_doc}}&&id_pos={{$id_pos}}" class="btn btn-success">
                            <i class="fa fa-file-excel-o"></i> EXPORTAR
                        </a>
                    </span>
                </div>
            </div>
        </form>
        <br>
        <div class="table-responsive">
            <table class="table table-striped mt-5">
                @if($cabecera!=null)
                <thead>
                    <tr>
                        @foreach($cabecera as $row)
                        <th>{{$row}}</th>
                        @endforeach
                    </tr>
                </thead>
                @endif
                <tbody>
                    @if($ventas!=null)
                    @foreach($ventas as $row)
                    <tr>
                        <td>1</td>
                        <td>{{date("d/m/Y", strtotime($row->fecha))}}</td>
                        {{-- <td>{{$row->tipo_tabla10}}</td> --}}
                        <td>{{$row->numero_de_serie}}</td>
                        <td>{{$row->correlativo}}</td>
                        {{-- <td>{{$row->tipo_tabla2}}</td> --}}
                        <td>{{$row->ruc}}</td>
                        <td>{{$row->nombre}}</td>
                        <td>{{round($row->neto,2)}}</td>
                        <td>{{round($row->igv,2)}}</td>
                        <td>{{round($row->importe_total,2)}}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>

            </table>
            @if($ventas!=null)
            {{ $ventas->appends(['fecha_inicio' => $fecha_inicio,'fecha_fin'=>$fecha_fin,'tipo_doc'=>$tipo_doc,'id_pos'=>$id_pos])->links() }}
            @endif
        </div>
    </div>
</div>
<script>
    // $('#fecha_inicio').mask("99/99/9999", {placeholder: 'MM/DD/YYYY' });
</script>
@endsection