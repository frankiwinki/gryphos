@extends('layouts.dashboard')
@section('content')
<div id="page-wrapper">
    <div class="header">
        <h3 class="page-header">
            REPORTE DE TANQUES
        </h3>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Reporte de Tanques</li>
        </ol>

    </div>
    <div id="page-inner">
        <form method="GET"  accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
            <div class="">
                <div class="form-group">
                    <div class="">
                        <label for="">Combustibles</label>
                    </div>
                    <div class="">
                        <select name="combustible" id="" class="form-control">
                            
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="">
                        <label for="">Fecha de Inicio </label>
                    </div>
                    <div class="">
                        <input type="date" class="form-control" name="fecha_inicio" value="{{ $fecha_inicio }}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="">
                        <label for="">Fecha de Fin </label>
                    </div>
                    <div class="">
                        <input type="date" class="form-control" name="fecha_fin" value="{{ $fecha_fin }}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="">
                        <label style="color: transparent">Botones</label>
                    </div>
                    <span class="input-group">
                        <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                    <span class="ml-2 input-group">
                        <a href="combustible_exportar?fecha_inicio={{$fecha_inicio}}&&fecha_fin={{$fecha_fin}}" class="btn btn-success">
                            <i class="fa fa-file-excel-o"></i> EXPORTAR
                        </a>
                    </span>
                </div>
            </div>
        </form>
        <br>

    </div>
</div>
<script>
    // $('#fecha_inicio').mask("99/99/9999", {placeholder: 'MM/DD/YYYY' });
</script>
@endsection