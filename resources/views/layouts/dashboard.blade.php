<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Gryphos-Dashboard</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/materialize/css/materialize.min.css')}}" media="screen,projection" />
    <!-- Bootstrap Styles-->
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="{{ asset('assets/css/font-awesome.css') }}" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
    <!-- Custom Styles-->
    <link href="{{ asset('assets/css/custom-styles.css')}}" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="{{ asset('assets/js/Lightweight-Chart/cssCharts.css')}}">
    <link href="{{ asset('css/star-rating.css') }}" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
     <script src="{{ asset('js/select2.js')}}"></script>
      <link href="{{ asset('css/select2.css') }}" rel="stylesheet" />
  


</head>

<body>
    <div id="wrapper">
        @include('layouts.partials.navbar-top')

        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li> 
                        <a class="active-menu waves-effect waves-dark" href="/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="/pos/users" class="waves-effect waves-dark"><i class="fa fa-desktop"></i>Usuarios</a>
                    </li>
                    <li>
                        <a href="/pos/clientes" class="waves-effect waves-dark"><i class="fa fa-desktop"></i>Clientes</a>
                    </li>

                    <li>
                        <a href="/pos/creditos" class="waves-effect waves-dark"><i class="fa fa-bar-chart-o"></i> Créditos</a>
                    </li>
                    <li>
                        <a href="/pos/productos" class="waves-effect waves-dark"><i class="fa fa-qrcode"></i> Productos</a>
                    </li>
                    <li>
                        <a href="/pos/descuentos" class="waves-effect waves-dark"><i class="fa fa-qrcode"></i> Descuentos</a>
                    </li>

                    <li>
                        <a href="#" class="waves-effect waves-dark"><i class="fa fa-sitemap"></i> Canjes<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="/pos/canjesproductos">Configuración</a>
                            </li>
                            <li>
                                <a href="/pos/canjes">Consultar</a>
                            </li>
                            <li>
                                <a href="/canjes/despacho">Despachar</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="waves-effect waves-dark">
                            <i class="fa fa-table"></i> Reportes<span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="/admin/reportes/buscar" class="waves-effect waves-dark">
                                    Ventas</a>
                            </li>
                            <li>
                                <a href="/admin/reportes/detalle" class="waves-effect waves-dark">
                                    Ventas Detalles</a>
                            </li>
                            <li>
                                <a href="/admin/reportes/combustibles">Ventas por combustible</a>
                            </li>
                            <li>
                                <a href="/admin/reportes/tanks">Tanques</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="waves-effect waves-dark">
                            <i class="fa fa-table"></i> Stock<span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="/admin/tanks" class="waves-effect waves-dark">
                                    Tanques</a>
                            </li>
                            <li>
                                <a href="/admin/varillaje">Varillajes</a>
                            </li>
                        </ul>
                    </li>
                    @if(Auth::user()->hasRole('admin'))

                    <li>
                        <a href="#" class="waves-effect waves-dark"><i class="fa fa-sitemap"></i> PrestaPuntos<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ route('prestapuntos.index') }}">Consultar</a>
                            </li>
                            <li>
                                <a href="#">Comprar</a>
                            </li>
                        </ul>
                    </li>
                    @endif
                </ul>

            </div>

        </nav>
        <div id="main">
            <!-- /. NAV SIDE  -->
            @yield('content')
        </div>

    </div>
    <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- jQuery Js -->
    
    <!-- Metis Menu Js -->
    <script src="{{ asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- Morris Chart Js -->
    <script src="{{ asset('assets/js/morris/raphael-2.1.0.min.js')}}"></script>
    <script src="{{ asset('assets/js/morris/morris.js')}}"></script>


    <script src="{{ asset('assets/js/easypiechart.js')}}"></script>
    <script src="{{ asset('assets/js/easypiechart-data.js')}}"></script>

    <script src="{{ asset('assets/js/Lightweight-Chart/jquery.chart.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('assets/js/custom-scripts.js')}}"></script>
    <script src="{{ asset('js/star-rating.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('.rating').rating('create', {
                displayOnly: true,
                showCaption: false,
                size: 'sm'

            });
        });
        $(document).ready(function() {
            //alert("jfndkd");
    $('.select').select2();
});
    </script>

</body>

</html>