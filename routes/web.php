<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Symfony\Component\Routing\Annotation\Route;
use Illuminate\Support\Facades\Route; 
 
Auth::routes();
Route::get('/', 'Admin\AdminController@index')->name('home');
Route::get('/home', 'Admin\AdminController@index')->name('home');
Route::get('admin', 'Admin\AdminController@index');
Route::get('admin/reportes','ReporteController@index');
Route::get('admin/reportes/buscar','ReporteController@buscarventas');
Route::get('admin/reportes/detalle','ReporteController@ventasdetalle');
Route::get('admin/reportes/combustibles','ReporteController@combustibles');
Route::get('admin/reportes/combustible_exportar','ReporteController@combustible_exportar');
Route::get('admin/reportes/exportar','ReporteController@exportar');
Route::get('admin/reportes_detalle/exportar','ReporteController@exportar_detalle')->name('exportar-detalle-reporte');
Route::get('pos/productos/exportar','ProductoController@exportar');
Route::get('pos/creditos/exportar','CreditoController@exportar');
Route::get('pos/vales/exportar','ValeController@exportar');

Route::resource('admin/roles', 'Admin\RolesController');
Route::resource('admin/permissions', 'Admin\PermissionsController');
Route::resource('admin/users', 'Admin\UsersController');
Route::resource('admin/pages', 'Admin\PagesController');
Route::resource('pos/users', 'UserPosController');
Route::resource('pos/clientes', 'ClientePosController');
Route::resource('admin/activitylogs', 'Admin\ActivityLogsController')->only([
    'index', 'show', 'destroy'
]);

// CAJES DE PRODUCTOS
Route::resource('/pos/canjesproductos', 'CanjesProductosController');
Route::get('/canjesproductos/detalle/{id}', 'CanjesProductosController@detalle');
Route::get('/canjesproductos/editar/{id}', 'CanjesProductosController@edit');



Route::resource('admin/settings', 'Admin\SettingsController');
Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

Route::resource('admin/business', 'Admin\\BusinessController');
Route::resource('admin/business', 'Admin\\BusinessController');
Route::resource('admin/business', 'Admin\\BusinessController');
 
Route::resource('admin/configurationB', 'Admin\\ConfigurationBController');
Route::resource('admin/configurationB', 'Admin\\ConfigurationBController');
Route::resource('admin/configurationB', 'Admin\\ConfigurationBController'); 
// Route::resource('pos/vales', 'ValeController');
Route::resource('pos/creditos', 'CreditoController');
Route::resource('pos/productos', 'ProductoController');
Route::resource('pos/caja', 'CajaController');
Route::resource('pos/clientes_sunat', 'ClienteSunatController');
Route::resource('pos/descuentos', 'DescuentoController');
Route::get('pos/descuentos/ver/{id_cliente}', 'DescuentoController@ver');
Route::resource('facturacion/comprobantes', 'ComprobantesController');
//Route::resource('pos/valesdetalle/{id_vale}', 'ValeController@detalle');
Route::get('pos/credito/{id_cliente}','CreditoController@credito_cliente');
Route::get('pos/cajaresumen','CajaController@generar')->name('resumen');
Route::get('dashboard','Admin\AdminController@index');
Route::get('pos/vales/listar/{id_credito}', 'ValeController@index');
Route::get('pos/vales/crear/{id_credito}', 'ValeController@create');
Route::get('pos/vales/editarpago/{id_credito}', 'ValeController@editarpago');

Route::post('pos/vales/save', 'ValeController@save'); 
 
Route::get('pos/vales/{id}', 'ValeController@show');

Route::get('pos/vales/infoPagos/{id}', 'ValeController@infoPagos');


Route::get('pos/vales/editar/{id}', 'ValeController@edit');
Route::get('pos/creditos/editar/{id}', 'CreditoController@edit'); 
Route::resource('pos/canjes', 'CanjesController');
Route::get('/canjes/despacho','CanjesController@despacho');
Route::get('/canjes/cliente/{num_doc}','CanjesController@cliente');
Route::get('/productos','ProductoController@listar');
Route::get('/canjes/detalle/{id}','CanjesController@detalle');

#Stock
Route::get('/admin/varillaje','StockController@varillaje');
Route::get('/admin/tanks','StockController@tanks');
Route::get('/admin/tanks/{id}','StockController@tanksEdit')->name('edit-tank');
Route::post('/tanks/store','StockController@tanks_store')->name('add-tank');
Route::post('/varillaje/store','StockController@varillaje_store')->name('add-varillaje');
Route::put('/tanks/update/{id}','StockController@tanks_update')->name('update-tank');


#Reportes
Route::get('/admin/reportes/tanks','StockController@tanks_report');


#ventas historico
Route::get('/ventas/detalle/{id}','CanjesController@detalleItems');
Route::get('/canjes/exportar','CanjesController@exportar');
Route::get('/canjes_detalle/exportar/{id}','CanjesController@detalles_exportar')->name('exportar-canjes-detalle');
 